package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.BookingEntity;

public class BookingEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public BookingEntity newBookingEntity() {

		Integer id = mockValues.nextInteger();

		BookingEntity bookingEntity = new BookingEntity();
		bookingEntity.setId(id);
		return bookingEntity;
	}
	
}
