package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.InterestEntity;

public class InterestEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public InterestEntity newInterestEntity() {

		Integer id = mockValues.nextInteger();

		InterestEntity interestEntity = new InterestEntity();
		interestEntity.setId(id);
		return interestEntity;
	}
	
}
