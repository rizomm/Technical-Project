package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.MusicEntity;

public class MusicEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public MusicEntity newMusicEntity() {

		Integer id = mockValues.nextInteger();

		MusicEntity musicEntity = new MusicEntity();
		musicEntity.setId(id);
		return musicEntity;
	}
	
}
