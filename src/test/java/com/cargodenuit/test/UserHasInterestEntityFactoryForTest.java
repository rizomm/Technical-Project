package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.UserHasInterestEntity;

public class UserHasInterestEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasInterestEntity newUserHasInterestEntity() {

		Integer userId = mockValues.nextInteger();
		Integer interestId = mockValues.nextInteger();

		UserHasInterestEntity userHasInterestEntity = new UserHasInterestEntity();
		userHasInterestEntity.setUserId(userId);
		userHasInterestEntity.setInterestId(interestId);
		return userHasInterestEntity;
	}
	
}
