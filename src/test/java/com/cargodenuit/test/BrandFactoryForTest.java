package com.cargodenuit.test;

import com.cargodenuit.entity.Brand;

public class BrandFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Brand newBrand() {

		Integer id = mockValues.nextInteger();

		Brand brand = new Brand();
		brand.setId(id);
		return brand;
	}
	
}
