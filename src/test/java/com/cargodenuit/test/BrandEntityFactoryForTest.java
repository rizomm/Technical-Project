package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.BrandEntity;

public class BrandEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public BrandEntity newBrandEntity() {

		Integer id = mockValues.nextInteger();

		BrandEntity brandEntity = new BrandEntity();
		brandEntity.setId(id);
		return brandEntity;
	}
	
}
