package com.cargodenuit.test;

import com.cargodenuit.entity.Kmcost;

public class KmcostFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Kmcost newKmcost() {

		Integer id = mockValues.nextInteger();

		Kmcost kmcost = new Kmcost();
		kmcost.setId(id);
		return kmcost;
	}
	
}
