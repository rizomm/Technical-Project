package com.cargodenuit.test;

import com.cargodenuit.entity.Booking;

public class BookingFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Booking newBooking() {

		Integer id = mockValues.nextInteger();

		Booking booking = new Booking();
		booking.setId(id);
		return booking;
	}
	
}
