package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.CarEntity;

public class CarEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CarEntity newCarEntity() {

		Integer id = mockValues.nextInteger();

		CarEntity carEntity = new CarEntity();
		carEntity.setId(id);
		return carEntity;
	}
	
}
