package com.cargodenuit.test;

import com.cargodenuit.entity.Music;

public class MusicFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Music newMusic() {

		Integer id = mockValues.nextInteger();

		Music music = new Music();
		music.setId(id);
		return music;
	}
	
}
