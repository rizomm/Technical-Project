package com.cargodenuit.test;

import com.cargodenuit.entity.Interest;

public class InterestFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Interest newInterest() {

		Integer id = mockValues.nextInteger();

		Interest interest = new Interest();
		interest.setId(id);
		return interest;
	}
	
}
