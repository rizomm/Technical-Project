package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.UserHasMusicEntity;

public class UserHasMusicEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasMusicEntity newUserHasMusicEntity() {

		Integer userId = mockValues.nextInteger();
		Integer musicId = mockValues.nextInteger();

		UserHasMusicEntity userHasMusicEntity = new UserHasMusicEntity();
		userHasMusicEntity.setUserId(userId);
		userHasMusicEntity.setMusicId(musicId);
		return userHasMusicEntity;
	}
	
}
