package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.FuelEntity;

public class FuelEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public FuelEntity newFuelEntity() {

		Integer id = mockValues.nextInteger();

		FuelEntity fuelEntity = new FuelEntity();
		fuelEntity.setId(id);
		return fuelEntity;
	}
	
}
