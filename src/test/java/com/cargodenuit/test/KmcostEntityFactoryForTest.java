package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.KmcostEntity;

public class KmcostEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public KmcostEntity newKmcostEntity() {

		Integer id = mockValues.nextInteger();

		KmcostEntity kmcostEntity = new KmcostEntity();
		kmcostEntity.setId(id);
		return kmcostEntity;
	}
	
}
