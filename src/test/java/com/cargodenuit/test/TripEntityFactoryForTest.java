package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.TripEntity;

public class TripEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public TripEntity newTripEntity() {

		Integer id = mockValues.nextInteger();

		TripEntity tripEntity = new TripEntity();
		tripEntity.setId(id);
		return tripEntity;
	}
	
}
