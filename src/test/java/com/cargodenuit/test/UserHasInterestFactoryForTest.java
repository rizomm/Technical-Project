package com.cargodenuit.test;

import com.cargodenuit.entity.UserHasInterest;

public class UserHasInterestFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasInterest newUserHasInterest() {

		Integer userId = mockValues.nextInteger();
		Integer interestId = mockValues.nextInteger();

		UserHasInterest userHasInterest = new UserHasInterest();
		userHasInterest.setUserId(userId);
		userHasInterest.setInterestId(interestId);
		return userHasInterest;
	}
	
}
