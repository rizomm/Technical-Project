package com.cargodenuit.test;

import com.cargodenuit.entity.Car;

public class CarFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Car newCar() {

		Integer id = mockValues.nextInteger();

		Car car = new Car();
		car.setId(id);
		return car;
	}
	
}
