package com.cargodenuit.test;

import com.cargodenuit.entity.UserHasMusic;

public class UserHasMusicFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasMusic newUserHasMusic() {

		Integer userId = mockValues.nextInteger();
		Integer musicId = mockValues.nextInteger();

		UserHasMusic userHasMusic = new UserHasMusic();
		userHasMusic.setUserId(userId);
		userHasMusic.setMusicId(musicId);
		return userHasMusic;
	}
	
}
