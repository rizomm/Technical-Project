package com.cargodenuit.test;

import com.cargodenuit.entity.Fuel;

public class FuelFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Fuel newFuel() {

		Integer id = mockValues.nextInteger();

		Fuel fuel = new Fuel();
		fuel.setId(id);
		return fuel;
	}
	
}
