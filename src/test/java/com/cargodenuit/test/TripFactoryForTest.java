package com.cargodenuit.test;

import com.cargodenuit.entity.Trip;

public class TripFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Trip newTrip() {

		Integer id = mockValues.nextInteger();

		Trip trip = new Trip();
		trip.setId(id);
		return trip;
	}
	
}
