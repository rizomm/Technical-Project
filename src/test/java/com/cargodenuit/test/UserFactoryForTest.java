package com.cargodenuit.test;

import com.cargodenuit.entity.User;

public class UserFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public User newUser() {

		Integer id = mockValues.nextInteger();

		User user = new User();
		user.setId(id);
		return user;
	}
	
}
