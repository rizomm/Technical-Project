package com.cargodenuit.test;

import com.cargodenuit.entity.Carmodel;

public class CarmodelFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Carmodel newCarmodel() {

		Integer id = mockValues.nextInteger();

		Carmodel carmodel = new Carmodel();
		carmodel.setId(id);
		return carmodel;
	}
	
}
