package com.cargodenuit.test;

import com.cargodenuit.entity.jpa.CarmodelEntity;

public class CarmodelEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CarmodelEntity newCarmodelEntity() {

		Integer id = mockValues.nextInteger();

		CarmodelEntity carmodelEntity = new CarmodelEntity();
		carmodelEntity.setId(id);
		return carmodelEntity;
	}
	
}
