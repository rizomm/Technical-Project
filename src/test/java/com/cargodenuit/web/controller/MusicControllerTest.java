package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Music;
import com.cargodenuit.entity.User;
import com.cargodenuit.test.MusicFactoryForTest;
import com.cargodenuit.test.UserFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.MusicService;
import com.cargodenuit.business.service.UserService;

//--- List Items 
import com.cargodenuit.web.listitem.UserListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class MusicControllerTest {
	
	@InjectMocks
	private MusicController musicController;
	@Mock
	private MusicService musicService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring

	private MusicFactoryForTest musicFactoryForTest = new MusicFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();

	List<User> users = new ArrayList<User>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Music> list = new ArrayList<Music>();
		when(musicService.findAll()).thenReturn(list);
		
		// When
		String viewName = musicController.list(model);
		
		// Then
		assertEquals("music/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = musicController.formForCreate(model);
		
		// Then
		assertEquals("music/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Music music = musicFactoryForTest.newMusic();
		Integer id = music.getId();
		when(musicService.findById(id)).thenReturn(music);
		
		// When
		String viewName = musicController.formForUpdate(model, id);
		
		// Then
		assertEquals("music/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(music, modelMap.get("music") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/music/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Music music = musicFactoryForTest.newMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Music musicCreated = new Music();
		when(musicService.create(music)).thenReturn(musicCreated); 
		
		// When
		String viewName = musicController.create(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/music/form/"+music.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musicCreated, modelMap.get("music") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Music music = musicFactoryForTest.newMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = musicController.create(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("music/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(music, modelMap.get("music") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/music/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Music music = musicFactoryForTest.newMusic();
		
		Exception exception = new RuntimeException("test exception");
		when(musicService.create(music)).thenThrow(exception);
		
		// When
		String viewName = musicController.create(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("music/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(music, modelMap.get("music") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/music/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "music.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Music music = musicFactoryForTest.newMusic();
		Integer id = music.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Music musicSaved = new Music();
		musicSaved.setId(id);
		when(musicService.update(music)).thenReturn(musicSaved); 
		
		// When
		String viewName = musicController.update(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/music/form/"+music.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musicSaved, modelMap.get("music") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Music music = musicFactoryForTest.newMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = musicController.update(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("music/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(music, modelMap.get("music") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/music/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Music music = musicFactoryForTest.newMusic();
		
		Exception exception = new RuntimeException("test exception");
		when(musicService.update(music)).thenThrow(exception);
		
		// When
		String viewName = musicController.update(music, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("music/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(music, modelMap.get("music") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/music/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "music.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Music music = musicFactoryForTest.newMusic();
		Integer id = music.getId();
		
		// When
		String viewName = musicController.delete(redirectAttributes, id);
		
		// Then
		verify(musicService).delete(id);
		assertEquals("redirect:/music", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Music music = musicFactoryForTest.newMusic();
		Integer id = music.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(musicService).delete(id);
		
		// When
		String viewName = musicController.delete(redirectAttributes, id);
		
		// Then
		verify(musicService).delete(id);
		assertEquals("redirect:/music", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "music.error.delete", exception);
	}
	
	
}
