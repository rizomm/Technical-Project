package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Car;
import com.cargodenuit.entity.Carmodel;
import com.cargodenuit.entity.Fuel;
import com.cargodenuit.test.CarFactoryForTest;
import com.cargodenuit.test.CarmodelFactoryForTest;
import com.cargodenuit.test.FuelFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.CarService;
import com.cargodenuit.business.service.CarmodelService;
import com.cargodenuit.business.service.FuelService;

//--- List Items 
import com.cargodenuit.web.listitem.CarmodelListItem;
import com.cargodenuit.web.listitem.FuelListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class CarControllerTest {
	
	@InjectMocks
	private CarController carController;
	@Mock
	private CarService carService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private CarmodelService carmodelService; // Injected by Spring
	@Mock
	private FuelService fuelService; // Injected by Spring

	private CarFactoryForTest carFactoryForTest = new CarFactoryForTest();
	private CarmodelFactoryForTest carmodelFactoryForTest = new CarmodelFactoryForTest();
	private FuelFactoryForTest fuelFactoryForTest = new FuelFactoryForTest();

	List<Carmodel> carmodels = new ArrayList<Carmodel>();
	List<Fuel> fuels = new ArrayList<Fuel>();

	private void givenPopulateModel() {
		Carmodel carmodel1 = carmodelFactoryForTest.newCarmodel();
		Carmodel carmodel2 = carmodelFactoryForTest.newCarmodel();
		List<Carmodel> carmodels = new ArrayList<Carmodel>();
		carmodels.add(carmodel1);
		carmodels.add(carmodel2);
		when(carmodelService.findAll()).thenReturn(carmodels);

		Fuel fuel1 = fuelFactoryForTest.newFuel();
		Fuel fuel2 = fuelFactoryForTest.newFuel();
		List<Fuel> fuels = new ArrayList<Fuel>();
		fuels.add(fuel1);
		fuels.add(fuel2);
		when(fuelService.findAll()).thenReturn(fuels);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Car> list = new ArrayList<Car>();
		when(carService.findAll()).thenReturn(list);
		
		// When
		String viewName = carController.list(model);
		
		// Then
		assertEquals("car/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}

	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Car car = carFactoryForTest.newCar();
		Integer id = car.getId();
		
		// When
		String viewName = carController.delete(redirectAttributes, id);
		
		// Then
		verify(carService).delete(id);
		assertEquals("redirect:/car", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Car car = carFactoryForTest.newCar();
		Integer id = car.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(carService).delete(id);
		
		// When
		String viewName = carController.delete(redirectAttributes, id);
		
		// Then
		verify(carService).delete(id);
		assertEquals("redirect:/car", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "car.error.delete", exception);
	}
	
	
}
