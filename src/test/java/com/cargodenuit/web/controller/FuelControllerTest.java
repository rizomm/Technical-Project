package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Fuel;
import com.cargodenuit.test.FuelFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.FuelService;


import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class FuelControllerTest {
	
	@InjectMocks
	private FuelController fuelController;
	@Mock
	private FuelService fuelService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private FuelFactoryForTest fuelFactoryForTest = new FuelFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Fuel> list = new ArrayList<Fuel>();
		when(fuelService.findAll()).thenReturn(list);
		
		// When
		String viewName = fuelController.list(model);
		
		// Then
		assertEquals("fuel/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = fuelController.formForCreate(model);
		
		// Then
		assertEquals("fuel/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Fuel)modelMap.get("fuel")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/fuel/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		Integer id = fuel.getId();
		when(fuelService.findById(id)).thenReturn(fuel);
		
		// When
		String viewName = fuelController.formForUpdate(model, id);
		
		// Then
		assertEquals("fuel/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuel, modelMap.get("fuel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/fuel/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Fuel fuelCreated = new Fuel();
		when(fuelService.create(fuel)).thenReturn(fuelCreated); 
		
		// When
		String viewName = fuelController.create(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/fuel/form/"+fuel.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuelCreated, modelMap.get("fuel") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = fuelController.create(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("fuel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuel, modelMap.get("fuel") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/fuel/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Fuel fuel = fuelFactoryForTest.newFuel();
		
		Exception exception = new RuntimeException("test exception");
		when(fuelService.create(fuel)).thenThrow(exception);
		
		// When
		String viewName = fuelController.create(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("fuel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuel, modelMap.get("fuel") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/fuel/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "fuel.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		Integer id = fuel.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Fuel fuelSaved = new Fuel();
		fuelSaved.setId(id);
		when(fuelService.update(fuel)).thenReturn(fuelSaved); 
		
		// When
		String viewName = fuelController.update(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/fuel/form/"+fuel.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuelSaved, modelMap.get("fuel") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = fuelController.update(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("fuel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuel, modelMap.get("fuel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/fuel/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Fuel fuel = fuelFactoryForTest.newFuel();
		
		Exception exception = new RuntimeException("test exception");
		when(fuelService.update(fuel)).thenThrow(exception);
		
		// When
		String viewName = fuelController.update(fuel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("fuel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(fuel, modelMap.get("fuel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/fuel/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "fuel.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		Integer id = fuel.getId();
		
		// When
		String viewName = fuelController.delete(redirectAttributes, id);
		
		// Then
		verify(fuelService).delete(id);
		assertEquals("redirect:/fuel", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Fuel fuel = fuelFactoryForTest.newFuel();
		Integer id = fuel.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(fuelService).delete(id);
		
		// When
		String viewName = fuelController.delete(redirectAttributes, id);
		
		// Then
		verify(fuelService).delete(id);
		assertEquals("redirect:/fuel", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "fuel.error.delete", exception);
	}
	
	
}
