package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Carmodel;
import com.cargodenuit.entity.Brand;
import com.cargodenuit.test.CarmodelFactoryForTest;
import com.cargodenuit.test.BrandFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.CarmodelService;
import com.cargodenuit.business.service.BrandService;

//--- List Items 
import com.cargodenuit.web.listitem.BrandListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class CarmodelControllerTest {
	
	@InjectMocks
	private CarmodelController carmodelController;
	@Mock
	private CarmodelService carmodelService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private BrandService brandService; // Injected by Spring

	private CarmodelFactoryForTest carmodelFactoryForTest = new CarmodelFactoryForTest();
	private BrandFactoryForTest brandFactoryForTest = new BrandFactoryForTest();

	List<Brand> brands = new ArrayList<Brand>();

	private void givenPopulateModel() {
		Brand brand1 = brandFactoryForTest.newBrand();
		Brand brand2 = brandFactoryForTest.newBrand();
		List<Brand> brands = new ArrayList<Brand>();
		brands.add(brand1);
		brands.add(brand2);
		when(brandService.findAll()).thenReturn(brands);

	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		Integer id = carmodel.getId();
		when(carmodelService.findById(id)).thenReturn(carmodel);
		
		// When
		String viewName = carmodelController.formForUpdate(model, id);
		
		// Then
		assertEquals("carmodel/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodel, modelMap.get("carmodel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/carmodel/update", modelMap.get("saveAction"));
		
		List<BrandListItem> brandListItems = (List<BrandListItem>) modelMap.get("listOfBrandItems");
		assertEquals(2, brandListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Carmodel carmodelCreated = new Carmodel();
		when(carmodelService.create(carmodel)).thenReturn(carmodelCreated); 
		
		// When
		String viewName = carmodelController.create(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/carmodel/form/"+carmodel.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodelCreated, modelMap.get("carmodel") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = carmodelController.create(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("carmodel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodel, modelMap.get("carmodel") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/carmodel/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<BrandListItem> brandListItems = (List<BrandListItem>) modelMap.get("listOfBrandItems");
		assertEquals(2, brandListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		
		Exception exception = new RuntimeException("test exception");
		when(carmodelService.create(carmodel)).thenThrow(exception);
		
		// When
		String viewName = carmodelController.create(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("carmodel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodel, modelMap.get("carmodel") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/carmodel/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "carmodel.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<BrandListItem> brandListItems = (List<BrandListItem>) modelMap.get("listOfBrandItems");
		assertEquals(2, brandListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		Integer id = carmodel.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Carmodel carmodelSaved = new Carmodel();
		carmodelSaved.setId(id);
		when(carmodelService.update(carmodel)).thenReturn(carmodelSaved); 
		
		// When
		String viewName = carmodelController.update(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/carmodel/form/"+carmodel.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodelSaved, modelMap.get("carmodel") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = carmodelController.update(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("carmodel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodel, modelMap.get("carmodel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/carmodel/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<BrandListItem> brandListItems = (List<BrandListItem>) modelMap.get("listOfBrandItems");
		assertEquals(2, brandListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		
		Exception exception = new RuntimeException("test exception");
		when(carmodelService.update(carmodel)).thenThrow(exception);
		
		// When
		String viewName = carmodelController.update(carmodel, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("carmodel/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(carmodel, modelMap.get("carmodel") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/carmodel/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "carmodel.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<BrandListItem> brandListItems = (List<BrandListItem>) modelMap.get("listOfBrandItems");
		assertEquals(2, brandListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		Integer id = carmodel.getId();
		
		// When
		String viewName = carmodelController.delete(redirectAttributes, id);
		
		// Then
		verify(carmodelService).delete(id);
		assertEquals("redirect:/carmodel", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Carmodel carmodel = carmodelFactoryForTest.newCarmodel();
		Integer id = carmodel.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(carmodelService).delete(id);
		
		// When
		String viewName = carmodelController.delete(redirectAttributes, id);
		
		// Then
		verify(carmodelService).delete(id);
		assertEquals("redirect:/carmodel", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "carmodel.error.delete", exception);
	}
	
	
}
