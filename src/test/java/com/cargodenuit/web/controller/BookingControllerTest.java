package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.User;
import com.cargodenuit.entity.Trip;
import com.cargodenuit.test.BookingFactoryForTest;
import com.cargodenuit.test.UserFactoryForTest;
import com.cargodenuit.test.TripFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.BookingService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.business.service.TripService;

//--- List Items 
import com.cargodenuit.web.listitem.UserListItem;
import com.cargodenuit.web.listitem.TripListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class BookingControllerTest {
	
	@InjectMocks
	private BookingController bookingController;
	@Mock
	private BookingService bookingService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring
	@Mock
	private TripService tripService; // Injected by Spring

	private BookingFactoryForTest bookingFactoryForTest = new BookingFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();
	private TripFactoryForTest tripFactoryForTest = new TripFactoryForTest();

	List<User> users = new ArrayList<User>();
	List<Trip> trips = new ArrayList<Trip>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

		Trip trip1 = tripFactoryForTest.newTrip();
		Trip trip2 = tripFactoryForTest.newTrip();
		List<Trip> trips = new ArrayList<Trip>();
		trips.add(trip1);
		trips.add(trip2);
		when(tripService.findAll()).thenReturn(trips);

	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Booking booking = bookingFactoryForTest.newBooking();
		Integer id = booking.getId();
		when(bookingService.findById(id)).thenReturn(booking);
		
		// When
		String viewName = bookingController.formForUpdate(model, id);
		
		// Then
		assertEquals("booking/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(booking, modelMap.get("booking") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/booking/update", modelMap.get("saveAction"));
		
		List<TripListItem> tripListItems = (List<TripListItem>) modelMap.get("listOfTripItems");
		assertEquals(2, tripListItems.size());
		
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Booking booking = bookingFactoryForTest.newBooking();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Booking bookingCreated = new Booking();
		when(bookingService.create(booking)).thenReturn(bookingCreated); 
		
		// When
		String viewName = bookingController.create(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/booking/form/"+booking.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(bookingCreated, modelMap.get("booking") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Booking booking = bookingFactoryForTest.newBooking();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = bookingController.create(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("booking/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(booking, modelMap.get("booking") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/booking/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TripListItem> tripListItems = (List<TripListItem>) modelMap.get("listOfTripItems");
		assertEquals(2, tripListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Booking booking = bookingFactoryForTest.newBooking();
		
		Exception exception = new RuntimeException("test exception");
		when(bookingService.create(booking)).thenThrow(exception);
		
		// When
		String viewName = bookingController.create(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("booking/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(booking, modelMap.get("booking") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/booking/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "booking.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TripListItem> tripListItems = (List<TripListItem>) modelMap.get("listOfTripItems");
		assertEquals(2, tripListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Booking booking = bookingFactoryForTest.newBooking();
		Integer id = booking.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Booking bookingSaved = new Booking();
		bookingSaved.setId(id);
		when(bookingService.update(booking)).thenReturn(bookingSaved); 
		
		// When
		String viewName = bookingController.update(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/booking/form/"+booking.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(bookingSaved, modelMap.get("booking") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Booking booking = bookingFactoryForTest.newBooking();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = bookingController.update(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("booking/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(booking, modelMap.get("booking") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/booking/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<TripListItem> tripListItems = (List<TripListItem>) modelMap.get("listOfTripItems");
		assertEquals(2, tripListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Booking booking = bookingFactoryForTest.newBooking();
		
		Exception exception = new RuntimeException("test exception");
		when(bookingService.update(booking)).thenThrow(exception);
		
		// When
		String viewName = bookingController.update(booking, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("booking/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(booking, modelMap.get("booking") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/booking/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "booking.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<TripListItem> tripListItems = (List<TripListItem>) modelMap.get("listOfTripItems");
		assertEquals(2, tripListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	
	
}
