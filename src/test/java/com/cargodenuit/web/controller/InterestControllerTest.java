package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Interest;
import com.cargodenuit.entity.User;
import com.cargodenuit.test.InterestFactoryForTest;
import com.cargodenuit.test.UserFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.InterestService;
import com.cargodenuit.business.service.UserService;

//--- List Items 
import com.cargodenuit.web.listitem.UserListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class InterestControllerTest {
	
	@InjectMocks
	private InterestController interestController;
	@Mock
	private InterestService interestService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring

	private InterestFactoryForTest interestFactoryForTest = new InterestFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();

	List<User> users = new ArrayList<User>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Interest> list = new ArrayList<Interest>();
		when(interestService.findAll()).thenReturn(list);
		
		// When
		String viewName = interestController.list(model);
		
		// Then
		assertEquals("interest/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Interest interest = interestFactoryForTest.newInterest();
		Integer id = interest.getId();
		when(interestService.findById(id)).thenReturn(interest);
		
		// When
		String viewName = interestController.formForUpdate(model, id);
		
		// Then
		assertEquals("interest/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interest, modelMap.get("interest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/interest/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Interest interest = interestFactoryForTest.newInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Interest interestCreated = new Interest();
		when(interestService.create(interest)).thenReturn(interestCreated); 
		
		// When
		String viewName = interestController.create(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/interest/form/"+interest.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interestCreated, modelMap.get("interest") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Interest interest = interestFactoryForTest.newInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = interestController.create(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("interest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interest, modelMap.get("interest") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/interest/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Interest interest = interestFactoryForTest.newInterest();
		
		Exception exception = new RuntimeException("test exception");
		when(interestService.create(interest)).thenThrow(exception);
		
		// When
		String viewName = interestController.create(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("interest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interest, modelMap.get("interest") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/interest/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "interest.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Interest interest = interestFactoryForTest.newInterest();
		Integer id = interest.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Interest interestSaved = new Interest();
		interestSaved.setId(id);
		when(interestService.update(interest)).thenReturn(interestSaved); 
		
		// When
		String viewName = interestController.update(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/interest/form/"+interest.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interestSaved, modelMap.get("interest") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Interest interest = interestFactoryForTest.newInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = interestController.update(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("interest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interest, modelMap.get("interest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/interest/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Interest interest = interestFactoryForTest.newInterest();
		
		Exception exception = new RuntimeException("test exception");
		when(interestService.update(interest)).thenThrow(exception);
		
		// When
		String viewName = interestController.update(interest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("interest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(interest, modelMap.get("interest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/interest/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "interest.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Interest interest = interestFactoryForTest.newInterest();
		Integer id = interest.getId();
		
		// When
		String viewName = interestController.delete(redirectAttributes, id);
		
		// Then
		verify(interestService).delete(id);
		assertEquals("redirect:/interest", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Interest interest = interestFactoryForTest.newInterest();
		Integer id = interest.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(interestService).delete(id);
		
		// When
		String viewName = interestController.delete(redirectAttributes, id);
		
		// Then
		verify(interestService).delete(id);
		assertEquals("redirect:/interest", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "interest.error.delete", exception);
	}
	
	
}
