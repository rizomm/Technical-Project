package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Kmcost;
import com.cargodenuit.entity.Fuel;
import com.cargodenuit.test.KmcostFactoryForTest;
import com.cargodenuit.test.FuelFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.KmcostService;
import com.cargodenuit.business.service.FuelService;

//--- List Items 
import com.cargodenuit.web.listitem.FuelListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class KmcostControllerTest {
	
	@InjectMocks
	private KmcostController kmcostController;
	@Mock
	private KmcostService kmcostService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private FuelService fuelService; // Injected by Spring

	private KmcostFactoryForTest kmcostFactoryForTest = new KmcostFactoryForTest();
	private FuelFactoryForTest fuelFactoryForTest = new FuelFactoryForTest();

	List<Fuel> fuels = new ArrayList<Fuel>();

	private void givenPopulateModel() {
		Fuel fuel1 = fuelFactoryForTest.newFuel();
		Fuel fuel2 = fuelFactoryForTest.newFuel();
		List<Fuel> fuels = new ArrayList<Fuel>();
		fuels.add(fuel1);
		fuels.add(fuel2);
		when(fuelService.findAll()).thenReturn(fuels);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Kmcost> list = new ArrayList<Kmcost>();
		when(kmcostService.findAll()).thenReturn(list);
		
		// When
		String viewName = kmcostController.list(model);
		
		// Then
		assertEquals("kmcost/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = kmcostController.formForCreate(model);
		
		// Then
		assertEquals("kmcost/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		
		@SuppressWarnings("unchecked")
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		Integer id = kmcost.getId();
		when(kmcostService.findById(id)).thenReturn(kmcost);
		
		// When
		String viewName = kmcostController.formForUpdate(model, id);
		
		// Then
		assertEquals("kmcost/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcost, modelMap.get("kmcost") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/kmcost/update", modelMap.get("saveAction"));
		
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Kmcost kmcostCreated = new Kmcost();
		when(kmcostService.create(kmcost)).thenReturn(kmcostCreated); 
		
		// When
		String viewName = kmcostController.create(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/kmcost/form/"+kmcost.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcostCreated, modelMap.get("kmcost") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = kmcostController.create(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("kmcost/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcost, modelMap.get("kmcost") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/kmcost/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		
		Exception exception = new RuntimeException("test exception");
		when(kmcostService.create(kmcost)).thenThrow(exception);
		
		// When
		String viewName = kmcostController.create(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("kmcost/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcost, modelMap.get("kmcost") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/kmcost/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "kmcost.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		Integer id = kmcost.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Kmcost kmcostSaved = new Kmcost();
		kmcostSaved.setId(id);
		when(kmcostService.update(kmcost)).thenReturn(kmcostSaved); 
		
		// When
		String viewName = kmcostController.update(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/kmcost/form/"+kmcost.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcostSaved, modelMap.get("kmcost") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = kmcostController.update(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("kmcost/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcost, modelMap.get("kmcost") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/kmcost/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		
		Exception exception = new RuntimeException("test exception");
		when(kmcostService.update(kmcost)).thenThrow(exception);
		
		// When
		String viewName = kmcostController.update(kmcost, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("kmcost/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(kmcost, modelMap.get("kmcost") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/kmcost/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "kmcost.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<FuelListItem> fuelListItems = (List<FuelListItem>) modelMap.get("listOfFuelItems");
		assertEquals(2, fuelListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		Integer id = kmcost.getId();
		
		// When
		String viewName = kmcostController.delete(redirectAttributes, id);
		
		// Then
		verify(kmcostService).delete(id);
		assertEquals("redirect:/kmcost", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Kmcost kmcost = kmcostFactoryForTest.newKmcost();
		Integer id = kmcost.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(kmcostService).delete(id);
		
		// When
		String viewName = kmcostController.delete(redirectAttributes, id);
		
		// Then
		verify(kmcostService).delete(id);
		assertEquals("redirect:/kmcost", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "kmcost.error.delete", exception);
	}
	
	
}
