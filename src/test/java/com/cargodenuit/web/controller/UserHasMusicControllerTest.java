package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.UserHasMusic;
import com.cargodenuit.test.UserHasMusicFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.UserHasMusicService;


import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class UserHasMusicControllerTest {
	
	@InjectMocks
	private UserHasMusicController userHasMusicController;
	@Mock
	private UserHasMusicService userHasMusicService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private UserHasMusicFactoryForTest userHasMusicFactoryForTest = new UserHasMusicFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<UserHasMusic> list = new ArrayList<UserHasMusic>();
		when(userHasMusicService.findAll()).thenReturn(list);
		
		// When
		String viewName = userHasMusicController.list(model);
		
		// Then
		assertEquals("userHasMusic/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = userHasMusicController.formForCreate(model);
		
		// Then
		assertEquals("userHasMusic/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((UserHasMusic)modelMap.get("userHasMusic")).getUserId());
		assertNull(((UserHasMusic)modelMap.get("userHasMusic")).getMusicId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasMusic/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		Integer userId = userHasMusic.getUserId();
		Integer musicId = userHasMusic.getMusicId();
		when(userHasMusicService.findById(userId, musicId)).thenReturn(userHasMusic);
		
		// When
		String viewName = userHasMusicController.formForUpdate(model, userId, musicId);
		
		// Then
		assertEquals("userHasMusic/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusic, modelMap.get("userHasMusic") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasMusic/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasMusic userHasMusicCreated = new UserHasMusic();
		when(userHasMusicService.create(userHasMusic)).thenReturn(userHasMusicCreated); 
		
		// When
		String viewName = userHasMusicController.create(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasMusic/form/"+userHasMusic.getUserId()+"/"+userHasMusic.getMusicId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusicCreated, modelMap.get("userHasMusic") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasMusicController.create(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasMusic/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusic, modelMap.get("userHasMusic") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasMusic/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasMusicService.create(userHasMusic)).thenThrow(exception);
		
		// When
		String viewName = userHasMusicController.create(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasMusic/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusic, modelMap.get("userHasMusic") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasMusic/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasMusic.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		Integer userId = userHasMusic.getUserId();
		Integer musicId = userHasMusic.getMusicId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasMusic userHasMusicSaved = new UserHasMusic();
		userHasMusicSaved.setUserId(userId);
		userHasMusicSaved.setMusicId(musicId);
		when(userHasMusicService.update(userHasMusic)).thenReturn(userHasMusicSaved); 
		
		// When
		String viewName = userHasMusicController.update(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasMusic/form/"+userHasMusic.getUserId()+"/"+userHasMusic.getMusicId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusicSaved, modelMap.get("userHasMusic") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasMusicController.update(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasMusic/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusic, modelMap.get("userHasMusic") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasMusic/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasMusicService.update(userHasMusic)).thenThrow(exception);
		
		// When
		String viewName = userHasMusicController.update(userHasMusic, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasMusic/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasMusic, modelMap.get("userHasMusic") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasMusic/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasMusic.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		Integer userId = userHasMusic.getUserId();
		Integer musicId = userHasMusic.getMusicId();
		
		// When
		String viewName = userHasMusicController.delete(redirectAttributes, userId, musicId);
		
		// Then
		verify(userHasMusicService).delete(userId, musicId);
		assertEquals("redirect:/userHasMusic", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasMusic userHasMusic = userHasMusicFactoryForTest.newUserHasMusic();
		Integer userId = userHasMusic.getUserId();
		Integer musicId = userHasMusic.getMusicId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(userHasMusicService).delete(userId, musicId);
		
		// When
		String viewName = userHasMusicController.delete(redirectAttributes, userId, musicId);
		
		// Then
		verify(userHasMusicService).delete(userId, musicId);
		assertEquals("redirect:/userHasMusic", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "userHasMusic.error.delete", exception);
	}
	
	
}
