package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Brand;
import com.cargodenuit.test.BrandFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.BrandService;


import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class BrandControllerTest {
	
	@InjectMocks
	private BrandController brandController;
	@Mock
	private BrandService brandService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private BrandFactoryForTest brandFactoryForTest = new BrandFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Brand> list = new ArrayList<Brand>();
		when(brandService.findAll()).thenReturn(list);
		
		// When
		String viewName = brandController.list(model);
		
		// Then
		assertEquals("brand/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = brandController.formForCreate(model);
		
		// Then
		assertEquals("brand/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Brand)modelMap.get("brand")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/brand/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Brand brand = brandFactoryForTest.newBrand();
		Integer id = brand.getId();
		when(brandService.findById(id)).thenReturn(brand);
		
		// When
		String viewName = brandController.formForUpdate(model, id);
		
		// Then
		assertEquals("brand/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brand, modelMap.get("brand") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/brand/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Brand brand = brandFactoryForTest.newBrand();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Brand brandCreated = new Brand();
		when(brandService.create(brand)).thenReturn(brandCreated); 
		
		// When
		String viewName = brandController.create(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/brand/form/"+brand.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brandCreated, modelMap.get("brand") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Brand brand = brandFactoryForTest.newBrand();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = brandController.create(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("brand/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brand, modelMap.get("brand") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/brand/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Brand brand = brandFactoryForTest.newBrand();
		
		Exception exception = new RuntimeException("test exception");
		when(brandService.create(brand)).thenThrow(exception);
		
		// When
		String viewName = brandController.create(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("brand/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brand, modelMap.get("brand") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/brand/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "brand.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Brand brand = brandFactoryForTest.newBrand();
		Integer id = brand.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Brand brandSaved = new Brand();
		brandSaved.setId(id);
		when(brandService.update(brand)).thenReturn(brandSaved); 
		
		// When
		String viewName = brandController.update(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/brand/form/"+brand.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brandSaved, modelMap.get("brand") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Brand brand = brandFactoryForTest.newBrand();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = brandController.update(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("brand/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brand, modelMap.get("brand") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/brand/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Brand brand = brandFactoryForTest.newBrand();
		
		Exception exception = new RuntimeException("test exception");
		when(brandService.update(brand)).thenThrow(exception);
		
		// When
		String viewName = brandController.update(brand, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("brand/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(brand, modelMap.get("brand") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/brand/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "brand.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Brand brand = brandFactoryForTest.newBrand();
		Integer id = brand.getId();
		
		// When
		String viewName = brandController.delete(redirectAttributes, id);
		
		// Then
		verify(brandService).delete(id);
		assertEquals("redirect:/brand", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Brand brand = brandFactoryForTest.newBrand();
		Integer id = brand.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(brandService).delete(id);
		
		// When
		String viewName = brandController.delete(redirectAttributes, id);
		
		// Then
		verify(brandService).delete(id);
		assertEquals("redirect:/brand", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "brand.error.delete", exception);
	}
	
	
}
