package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.Trip;
import com.cargodenuit.entity.User;
import com.cargodenuit.test.TripFactoryForTest;
import com.cargodenuit.test.UserFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.TripService;
import com.cargodenuit.business.service.UserService;

//--- List Items 
import com.cargodenuit.web.listitem.UserListItem;

import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class TripControllerTest {
	
	@InjectMocks
	private TripController tripController;
	@Mock
	private TripService tripService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring

	private TripFactoryForTest tripFactoryForTest = new TripFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();

	List<User> users = new ArrayList<User>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Trip> list = new ArrayList<Trip>();
		when(tripService.findAll()).thenReturn(list);
		
		// When
		String viewName = tripController.list(model);
		
		// Then
		assertEquals("trip/list", viewName);
		Map<String,?> modelMap = model.asMap();

	}
	
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Trip trip = tripFactoryForTest.newTrip();
		Integer id = trip.getId();
		when(tripService.findById(id)).thenReturn(trip);
		
		// When
		String viewName = tripController.formForUpdate(model, id);
		
		// Then
		assertEquals("trip/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(trip, modelMap.get("trip") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/trip/update", modelMap.get("saveAction"));
		
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Trip trip = tripFactoryForTest.newTrip();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = tripController.create(trip, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("trip/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(trip, modelMap.get("trip") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/trip/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Trip trip = tripFactoryForTest.newTrip();
		
		Exception exception = new RuntimeException("test exception");
		when(tripService.create(trip)).thenThrow(exception);
		
		// When
		String viewName = tripController.create(trip, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("trip/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(trip, modelMap.get("trip") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/trip/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "trip.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Trip trip = tripFactoryForTest.newTrip();
		Integer id = trip.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Trip tripSaved = new Trip();
		tripSaved.setId(id);
		when(tripService.update(trip)).thenReturn(tripSaved); 
		
		// When
		String viewName = tripController.update(trip, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/trip/form/"+trip.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(tripSaved, modelMap.get("trip") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Trip trip = tripFactoryForTest.newTrip();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = tripController.update(trip, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("trip/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(trip, modelMap.get("trip") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/trip/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Trip trip = tripFactoryForTest.newTrip();
		
		Exception exception = new RuntimeException("test exception");
		when(tripService.update(trip)).thenThrow(exception);
		
		// When
		String viewName = tripController.update(trip, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("trip/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(trip, modelMap.get("trip") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/trip/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "trip.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Trip trip = tripFactoryForTest.newTrip();
		Integer id = trip.getId();
		
		// When
		String viewName = tripController.delete(redirectAttributes, id);
		
		// Then
		verify(tripService).delete(id);
		assertEquals("redirect:/trip", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Trip trip = tripFactoryForTest.newTrip();
		Integer id = trip.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(tripService).delete(id);
		
		// When
		String viewName = tripController.delete(redirectAttributes, id);
		
		// Then
		verify(tripService).delete(id);
		assertEquals("redirect:/trip", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "trip.error.delete", exception);
	}
	
	
}
