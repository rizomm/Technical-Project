package com.cargodenuit.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.cargodenuit.entity.UserHasInterest;
import com.cargodenuit.test.UserHasInterestFactoryForTest;

//--- Services 
import com.cargodenuit.business.service.UserHasInterestService;


import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageHelper;
import com.cargodenuit.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class UserHasInterestControllerTest {
	
	@InjectMocks
	private UserHasInterestController userHasInterestController;
	@Mock
	private UserHasInterestService userHasInterestService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private UserHasInterestFactoryForTest userHasInterestFactoryForTest = new UserHasInterestFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<UserHasInterest> list = new ArrayList<UserHasInterest>();
		when(userHasInterestService.findAll()).thenReturn(list);
		
		// When
		String viewName = userHasInterestController.list(model);
		
		// Then
		assertEquals("userHasInterest/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = userHasInterestController.formForCreate(model);
		
		// Then
		assertEquals("userHasInterest/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((UserHasInterest)modelMap.get("userHasInterest")).getUserId());
		assertNull(((UserHasInterest)modelMap.get("userHasInterest")).getInterestId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasInterest/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		Integer userId = userHasInterest.getUserId();
		Integer interestId = userHasInterest.getInterestId();
		when(userHasInterestService.findById(userId, interestId)).thenReturn(userHasInterest);
		
		// When
		String viewName = userHasInterestController.formForUpdate(model, userId, interestId);
		
		// Then
		assertEquals("userHasInterest/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterest, modelMap.get("userHasInterest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasInterest/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasInterest userHasInterestCreated = new UserHasInterest();
		when(userHasInterestService.create(userHasInterest)).thenReturn(userHasInterestCreated); 
		
		// When
		String viewName = userHasInterestController.create(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasInterest/form/"+userHasInterest.getUserId()+"/"+userHasInterest.getInterestId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterestCreated, modelMap.get("userHasInterest") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasInterestController.create(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasInterest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterest, modelMap.get("userHasInterest") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasInterest/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasInterestService.create(userHasInterest)).thenThrow(exception);
		
		// When
		String viewName = userHasInterestController.create(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasInterest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterest, modelMap.get("userHasInterest") );
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasInterest/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasInterest.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		Integer userId = userHasInterest.getUserId();
		Integer interestId = userHasInterest.getInterestId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasInterest userHasInterestSaved = new UserHasInterest();
		userHasInterestSaved.setUserId(userId);
		userHasInterestSaved.setInterestId(interestId);
		when(userHasInterestService.update(userHasInterest)).thenReturn(userHasInterestSaved); 
		
		// When
		String viewName = userHasInterestController.update(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasInterest/form/"+userHasInterest.getUserId()+"/"+userHasInterest.getInterestId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterestSaved, modelMap.get("userHasInterest") );
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasInterestController.update(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasInterest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterest, modelMap.get("userHasInterest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasInterest/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasInterestService.update(userHasInterest)).thenThrow(exception);
		
		// When
		String viewName = userHasInterestController.update(userHasInterest, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasInterest/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasInterest, modelMap.get("userHasInterest") );
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasInterest/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasInterest.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		Integer userId = userHasInterest.getUserId();
		Integer interestId = userHasInterest.getInterestId();
		
		// When
		String viewName = userHasInterestController.delete(redirectAttributes, userId, interestId);
		
		// Then
		verify(userHasInterestService).delete(userId, interestId);
		assertEquals("redirect:/userHasInterest", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasInterest userHasInterest = userHasInterestFactoryForTest.newUserHasInterest();
		Integer userId = userHasInterest.getUserId();
		Integer interestId = userHasInterest.getInterestId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(userHasInterestService).delete(userId, interestId);
		
		// When
		String viewName = userHasInterestController.delete(redirectAttributes, userId, interestId);
		
		// Then
		verify(userHasInterestService).delete(userId, interestId);
		assertEquals("redirect:/userHasInterest", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "userHasInterest.error.delete", exception);
	}
	
	
}
