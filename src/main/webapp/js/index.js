/* AUTHOR: Valentin ALBERT*/
/* EMAIL: valentin.albert.pro@gmail.com */
/* HOMEMADE TOOLTIP SYSTEM */

jQuery(document).ready(function(){
	/* Tipper funcions*/
	var tippers = jQuery('.dataTipper');

	/* Car animation*/
	jQuery('.carsolo').animate({
		top: '130px',
		width: '250px',
		left: '-150px'
	}, 2500, function(){
		jQuery('.bubble').fadeIn(400);
	});

	tippers.click(function(e){
		e.preventDefault();
		var target = jQuery('#' + jQuery(this).attr("data-target"));
		var isTrigger = jQuery(this).attr("is-trigger");
		var toggler = (isTrigger == "true" ? jQuery('#' + jQuery(this).attr("position-referent")) : jQuery(this));

		tipperInit(toggler, target);
		target.stop().fadeToggle(300);
	});

		jQuery('.maincontainer').click(function(e) {
	  if (jQuery(e.target).closest('.dataTipper').length === 0) {
	  	jQuery('.tip').stop().fadeOut(300);
	  }
	});

	/* Calculate the exact horizontal position of the tooltip */
	function offsetLeftCalculation(toggler, centerOn){
		var reference = (centerOn == "parent" ? toggler.parent() : toggler);
		var totalOffset = reference.offset().left;
		var semiRefWidth = reference.width() / 2;
		var adjust = 6;
		var finalOffset = (totalOffset - semiRefWidth) + adjust;

		return finalOffset.toString() + "px";
	};

	/* Tipper init on page load - calculate positions and stuff */
	function tipperInit(toggler, target){
		var centerOn = toggler.attr("center-on");
		var tipPositionY = (toggler.offset().top + 70).toString() + "px";
		var tipPositionX = offsetLeftCalculation(toggler, centerOn);

		target.css("position", "absolute");
		target.css("left", tipPositionX);
		target.css("top", tipPositionY);
	};


	jQuery(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
  		var modal = jQuery(this);
  		jQuery('.colorpick').colorpicker({
    		displayIndicator: false
		});

	});

	var ratingsHisto = jQuery('.jRateHistory');
	var ratingsProfil = jQuery('.jRateProfil');

	ratingsProfil.each(function(){
		var driverRate = jQuery(this).attr("data-rate");

		jQuery(this).jRate({
			rating: parseInt(driverRate),
			readOnly: true,
			startColor: '#29649D',
			endColor: '#29649D',
			precision: 2
		});
	});

	ratingsHisto.each(function(){
		var driverRate = jQuery(this).attr("data-rate");

		jQuery(this).jRate({
			rating: parseInt(driverRate),
			readOnly: true,
			startColor: '#669ACC',
			endColor: '#669ACC',
			precision: 2
		});
	});

    $(function () {
    	
    	// On innititalise le champ car_issuancedate et on lui indique que la date max est la date du jour		
        $('#car_issuancedate').fdatetimepicker({
            language: 'fr',
            format: 'dd/mm/yyyy',
            endDate: new Date(),
        }).on('changeDate', function (ev) {
		      date 				= $('#car_issuancedate').val().split("/");
		      inssuancedate 	= new Date(date[2], date[1]-1, date[0], 0, 0);
		      			      
		      var d = inssuancedate.getDate();
		      if (d < 10) {
		          d = "0" + d;
		      }
		      var m =  inssuancedate.getMonth();
		      m += 1;
		      if (m < 10) {
		          m = "0" + m;
		      }

		      $("#car_issuancedate_hidden").val(m + "/" + d + "/" + inssuancedate.getFullYear() + " 7:04 PM");
        });
    });

});