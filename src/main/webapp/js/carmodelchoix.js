function CarModelChoix(form) { 
	
	pathArray = location.href.split( '/' );
	protocol = pathArray[0];
	host = pathArray[2];
	appname = pathArray[3];
	baseurl = protocol + '//' + host + '/' + appname ;
	
	brandid = form.Brand.selectedIndex; 

    $.ajax({
    	url: baseurl + "/car/majCarModel/"+brandid, 
    	success: function(result){
    		$("#car_model_form_div").html(result);
    	}
    });
} 