	pathArray 	= location.href.split( '/' );
	route 		= pathArray[4];
	action 		= pathArray[5];
		

		var directionsDisplay;
		var directionsResult;
		
		var directionsService = new google.maps.DirectionsService({directions : directionsResult});
		var map;
		
		function initialize() {
		  
		  directionsDisplay = new google.maps.DirectionsRenderer();
		  var flst = new google.maps.LatLng(50.63340, 3.04496);
		  var mapOptions = {
		    zoom:15,
		    center: flst
		  };

		  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		  directionsDisplay.setMap(map);
		  
		}
		
		function calcRoute() {
			
		initialize();
		  var start = document.getElementById('start').value;
		  var end = document.getElementById('end').value;
		  var request = {
		      origin:start,
		      destination:end,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  
		    
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		      var legs = response.routes[0].legs;
		      
		      var totalDistance = 0;
		      var totalDuration = 0;
		      var startloc = "";
		      var endloc = "";
		      
		      
		      for(var i=0; i<legs.length; ++i) {
		          totalDistance += legs[i].distance.value;
		          totalDuration += legs[i].duration.value;
		          
		          startloc  += legs[i].start_location;
		          endloc 	+= legs[i].end_location;
		                
		      }
		      
		      	// Latitude et longitude de départ
			      LatLng_start = startloc.split( ',' );
			      
			      lat_start 	= LatLng_start[0];
			      lat_start		= lat_start.split( '(' );
			      lat_start		= lat_start[1];
			      
			      long_start	= LatLng_start[1];
			      long_start	= long_start.split( ')' );
			      long_start	= long_start[0];
			      
			   // Latitude et longitude d'arrivée
			      
			      LatLng_end = endloc.split( ',' );
			      
			      lat_end 		= LatLng_end[0];
			      lat_end		= lat_end.split( '(' );
			      lat_end		= lat_end[1];
			      
			      long_end		= LatLng_end[1];
			      long_end		= long_end.split( ')' );
			      long_end		= long_end[0];
		      
			  
		      var km = Math.floor(totalDistance/1000);
		      $('#distance').val(km + " kms");
		      $('#distance_hidden').val(km);
		      
		      var cost = $('#cost_per_km').val() * km ;
		      $('#cost').val(cost + "€");
		      $('#trip_cost').val(cost);
		      
		      
		      $('#trip_latitudestart').val(lat_start);
		      $('#trip_longitudestart').val(long_start);
		      
		      $('#trip_latitudeend').val(lat_end);
		      $('#trip_longitudeend').val(long_end);
		      
		      
		      startdate 				= $('#trip_startdate').val().split("/");
		      
		      startdate_year_and_time 	= startdate[2].split(" ");
		      
		      time 						= startdate_year_and_time[1].split(":");
		      startminute				= time[1] ;
		      starthour					= time[0] ;
		      
		      
		      var local = location.href.substr(location.href.length - 5) ;
		      
		      enddate 	= new Date(startdate_year_and_time[0], startdate[1]-1, startdate[0], starthour, startminute);
		     
		      	    	  
		      enddate.setSeconds(enddate.getSeconds() + totalDuration);
		      
		      
		      var d = enddate.getDate();
		      if (d < 10) {
		          d = "0" + d;
		      }
		      var m =  enddate.getMonth();
		      m += 1;
		      if (m < 10) {
		          m = "0" + m;
		      }
		      var y = enddate.getFullYear();
		      
		      var h = enddate.getHours();
		      if (h < 10) {
		          h = "0" + h;
		      }
		      var min = enddate.getMinutes();
		      if (min < 10) {
		    	  min = "0" + min;
		      }
		      var s = enddate.getSeconds();
		      if (s < 10) {
		          s = "0" + s;
		      }
		      

		    $("#trip_enddate").val(d + "/" + m + "/" + y + " " + h + ":" + min);
		    $("#trip_enddate_hidden").val(d + "/" + m + "/" + y + " " + h + ":" + min);

		      
		   // calculate (and subtract) whole days
		      var days = Math.floor(totalDuration / 86400);
		      totalDuration -= days * 86400;

		      // calculate (and subtract) whole hours
		      var hours = Math.floor(totalDuration / 3600) % 24;
		      totalDuration -= hours * 3600;
		      // calculate (and subtract) whole minutes
		      var minutes = Math.floor(totalDuration / 60) % 60;
		      totalDuration -= minutes * 60;

		      // what's left is seconds
		      var seconds = totalDuration % 60;  // in theory the modulus is not required
		      
		      var displayDuration = "";
		      
			      if(days != 0){
			    	  if(days > 1){
			    		  displayDuration += days + " jours " ;
			    	  }
			    	  else{
			    		  displayDuration += days + " jour " ;
			    	  }
			      }
			      if(hours != 0){
			    	  displayDuration += hours + " h " ; 
			      }
			      if(minutes != 0){
			    	  displayDuration += minutes + " min "; 
			      }
			      
			      $("#totalDuration").val(displayDuration);
			      
				geocoder = new google.maps.Geocoder();
				var tripStart = new google.maps.LatLng(lat_start, long_start); // Point de depart du trajet
				geocoder.geocode({'latLng': tripStart}, function(results, status) {
	        	    if (status == google.maps.GeocoderStatus.OK) {
	        	      if (results[1]) {
	        	    	  $("#trip_addressStart").val(results[1].formatted_address);
	        	      }
	        	    }
	            });
				
				var tripEnd = new google.maps.LatLng(lat_end, long_end); // Point de depart du trajet
				geocoder.geocode({'latLng': tripEnd}, function(results2, status2) {
	        	    if (status2 == google.maps.GeocoderStatus.OK) {
	        	      if (results2[1]) {
	        	    	  $("#trip_addressEnd").val(results2[1].formatted_address);
	        	      }
	        	    }
	            });

		    }
		  });
		  
		  document.getElementById("hidden_div").style.display = "block";
		  //$("# save_trip_form_buton").removeAttr("disabled");      
		
		}


		function searchTrip(nbTrips) {
			
			initialize();
			  var start = document.getElementById('checkpoint1').value;
			  var end = document.getElementById('checkpoint2').value;
			  var request = {
			      origin:start,
			      destination:end,
			      travelMode: google.maps.TravelMode.DRIVING
			  };
			  
			    
			  directionsService.route(request, function(response, status) {
			    if (status == google.maps.DirectionsStatus.OK) {
			      directionsDisplay.setDirections(response);
			      var legs = response.routes[0].legs;
			      
			      var totalDistance = 0;
			      var totalDuration = 0;
			      var startloc = "";
			      var endloc = "";
			      
			      for(var i=0; i<legs.length; ++i) {
			          totalDistance += legs[i].distance.value;
			          totalDuration += legs[i].duration.value;
			          
			          startloc  += legs[i].start_location;
			          endloc 	+= legs[i].end_location;
			      }
			      
			      	// Latitude et longitude de départ
				      LatLng_start = startloc.split( ',' );
				      
				      lat_start 	= LatLng_start[0];
				      lat_start		= lat_start.split( '(' );
				      lat_start		= lat_start[1];
				      
				      long_start	= LatLng_start[1];
				      long_start	= long_start.split( ')' );
				      long_start	= long_start[0];
				      
				   // Latitude et longitude d'arrivée
				      
				      LatLng_end = endloc.split( ',' );
				      
				      lat_end 		= LatLng_end[0];
				      lat_end		= lat_end.split( '(' );
				      lat_end		= lat_end[1];
				      
				      long_end		= LatLng_end[1];
				      long_end		= long_end.split( ')' );
				      long_end		= long_end[0];
			      
				  
			      var km = Math.floor(totalDistance/1000);
			      
			      $('#searchtrip_distance').val(km + " kms");
			      document.getElementById("searchtrip_distance_div").style.display = "block";
			      
			      startdate 				= $('#startdate').val().split("/");
			      
			      startdate_year_and_time 	= startdate[2].split(" ");
			      
			      time 						= startdate_year_and_time[1].split(":");
			      startminute				= time[1] ;
			      starthour					= time[0] ;
			      
			      
			      enddate 	= new Date(startdate_year_and_time[0], startdate[1], startdate[0], starthour, startminute);
			      	    	  
			      enddate.setSeconds(enddate.getSeconds() + totalDuration);
			      
			      
			      var d = enddate.getDate();
			      if (d < 10) {
			          d = "0" + d;
			      }
			      var m =  enddate.getMonth();
			      m += 1;
			      if (m < 10) {
			          m = "0" + m;
			      }
			      var y = enddate.getFullYear();
			      
			      var h = enddate.getHours();
			      if (h < 10) {
			          h = "0" + h;
			      }
			      var min = enddate.getMinutes();
			      if (min < 10) {
			    	  min = "0" + min;
			      }
			      var s = enddate.getSeconds();
			      if (s < 10) {
			          s = "0" + s;
			      }
		
			     $("#searchtrip_enddate_hidden").val(y + "-" + m + "-" + d + " " + h + ":" + min);
			     $("#searchtrip_enddate").val(d + "/" + m + "/" + y + " " + h + ":" + min);
			     document.getElementById("searchtrip_enddate_div").style.display = "block";
			      
			   // calculate (and subtract) whole days
			      var days = Math.floor(totalDuration / 86400);
			      totalDuration -= days * 86400;
		
			      // calculate (and subtract) whole hours
			      var hours = Math.floor(totalDuration / 3600) % 24;
			      totalDuration -= hours * 3600;
		
			      // calculate (and subtract) whole minutes
			      var minutes = Math.floor(totalDuration / 60) % 60;
			      totalDuration -= minutes * 60;
		
			      // what's left is seconds
			      var seconds = totalDuration % 60;  // in theory the modulus is not required
			      
			      var displayDuration = "";
			      
				      if(days != 0){
				    	  if(days > 1){
				    		  displayDuration += days + " jours " ;
				    	  }
				    	  else{
				    		  displayDuration += days + " jour " ;
				    	  }
				      }
				      if(hours != 0){
				    	  displayDuration += hours + " h " ; 
				      }
				      if(minutes != 0){
				    	  displayDuration += minutes + " min "; 
				      }
				      
				      $('#searchtrip_totalDuration').val(displayDuration);
				      document.getElementById("searchtrip_totalDuration_div").style.display = "block";
				      
			    }
			    
				  		  
				    function next(j) {
			        	  var tripStart = new google.maps.LatLng($("#latitudestart_"+j).text(), $("#longitudestart_"+j).text()); // Point de depart du trajet dans la liste des trajets existant
						  var tripEnd = new google.maps.LatLng($("#latitudeend_"+j).text(), $("#longitudeend_"+j).text()); // Point d'arrivée du trajet 
						  var tripNumberofseat = $("#numberofseat_"+j).text(); // Nombre de places total du trajet 
						  var bookingNumberofseat = $("#numberOfSeat").val(); // Nombre de places voulues
						  var requestOfTrip = {
							      origin:tripStart,
							      destination:tripEnd,
							      travelMode: google.maps.TravelMode.DRIVING
						  };

						  var bookingStart = new google.maps.LatLng(lat_start, long_start); // Point de depart recherché
						  var bookingEnd = new google.maps.LatLng(lat_end, long_end); // Point d'arrivée recherché
						  
						 directionsService.route(requestOfTrip, function(directionOfTrip, statusOfTrip) {
							
							  var isBookingStartInTrip = google.maps.geometry.poly.isLocationOnEdge(bookingStart,new google.maps.Polyline({path:google.maps.geometry.encoding.decodePath(directionOfTrip.routes[0].overview_polyline)}),0.01);
							  var isBookingEndInTrip = google.maps.geometry.poly.isLocationOnEdge(bookingEnd,new google.maps.Polyline({path:google.maps.geometry.encoding.decodePath(directionOfTrip.routes[0].overview_polyline)}),0.01);
							  	
							  // Startdate sans l'heure
							  startdateWithoutTime 		= $('#startdate').val().split(" ") ;
							  startdateWithoutTime 		= startdateWithoutTime[0] ;
							  
							  startdateTripWithoutTime 	= $('#startdate_'+j).text().split(" ") ;
							  startdateTripWithoutTime 	= startdateTripWithoutTime[0] ;
							  
							  
							  geocoder = new google.maps.Geocoder();

							  if(isBookingStartInTrip && isBookingEndInTrip && (startdateWithoutTime == startdateTripWithoutTime) && (tripNumberofseat >= bookingNumberofseat)){
								 $(".tr_"+j).show();
								 
								 var tripStart = new google.maps.LatLng($("#latitudestart_"+j).text(), $("#longitudestart_"+j).text());
								 
						             geocoder.geocode({'latLng': tripStart}, function(results, status) {
						            	    if (status == google.maps.GeocoderStatus.OK) {
						            	      if (results[1]) {
						            	    	  $("#addressstart_"+j).text(results[1].formatted_address);
						            	    	  var tripEnd = new google.maps.LatLng($("#latitudeend_"+j).text(), $("#longitudeend_"+j).text());
										             geocoder.geocode({'latLng': tripEnd}, function(results2, status2) {
										            	    if (status2 == google.maps.GeocoderStatus.OK) {
										            	      if (results2[1]) {
										            	    	  $("#addressend_"+j).text(results2[1].formatted_address);
										            	      }
										            	    }
										             });
						            	      } else {
						            	        //alert('No results found');
						            	      }
						            	    }
						            });
					             
							  }
							  
						  });
				    }
				    
				    for (var j = 0; j<nbTrips; j++) {
				    	$(".tr_"+j).hide();
				    
				    }		
				    
				    for (var j = 0; j<nbTrips; j++) {
				    	next(j);
				    }
				    
			  });  
			
			}