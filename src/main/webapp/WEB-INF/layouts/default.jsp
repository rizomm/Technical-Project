<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<jsp:output doctype-root-element="HTML" doctype-system="about:legacy-compat"/>
<jsp:directive.page contentType="text/html;charset=UTF-8"/>
<jsp:directive.page pageEncoding="UTF-8"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="cache-control" content="no-cache"/>

    <!-- Get the user locale from the page context (it was set by Spring MVC's locale resolver) -->
    <c:set var="plocale">${pageContext.response.locale}</c:set>
    <c:set var="userLocale">
        <c:out value="${fn:replace(plocale, '_', '-')}" default="en"/>
    </c:set>

    <spring:message code="application_name" var="app_name" htmlEscape="false"/>
    <title><c:out value="${app_name}"/></title>

    <!-- style -->
    <spring:url value="/css/bootstrap.min.css" var="css_url_bootstrap"/>
    <spring:url value="/css/bootstrap-theme.min.css" var="css_url_bootstrap_theme"/>
    <spring:url value="/css/datepicker3.css" var="css_url_datepicker"/>
    <spring:url value="/css/styles.css" var="css_url_styles"/>
    <spring:url value="/css/bootstrap-datetimepicker.css" var="css_url_datetimepicker"/>
    <spring:url value="/css/font-awesome.min.css" var="css_url_fontawesome"/>
    <spring:url value="/css/bootstrap-multiselect.css" var="css_url_bootstrap_multiselect"/>
    <spring:url value="/css/ios_checkbox/style.css" var="css_url_ios_checkbox"/>
    <link href="${css_url_bootstrap}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_bootstrap_theme}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_datepicker}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_styles}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_datetimepicker}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_fontawesome}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_bootstrap_multiselect}" rel="stylesheet" type="text/css"/>
    <link href="${css_url_ios_checkbox}" rel="stylesheet" type="text/css"/>

    <style>
        html, body, #map-canvas {
            height: 100%;
            left: 3%;
        }
    </style>


    <!-- javascript -->

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBIHXK_2ph_qwnua5FDDhhrKzAfhcSmlU">
    </script>

    <spring:url value="/js/validation.js" var="js_url_validation"/>
    <spring:url value="/js/jquery-1.10.2.min.js" var="js_url_jquery"/>
    <spring:url value="/js/bootstrap.js" var="js_url_bootstrap"/>
    <spring:url value="/js/bootstrap-datepicker.js" var="js_url_bootstrap_datepicker"/>
    <spring:url value="/js/locales/bootstrap-datepicker.${fn:substring(plocale, 0, 2)}.js"
                var="js_url_bootstrap_datepicker_locale"/>
    <spring:url value="/js/carmodelchoix.js" var="js_url_carmodelchoix"/>
    <spring:url value="/js/googlemap.js" var="js_url_googlemap"/>
    <spring:url value="/js/bootstrap-datetimepicker.js" var="js_url_datetimepicker"/>
    <spring:url value="/js/moment-with-locales.js" var="js_url_moment_js"/>
    <spring:url value="/js/bootstrap-multiselect.js" var="js_url_bootstrap_multiselect"/>
    <spring:url value="/js/iphone-style-checkboxes.js" var="js_url_iphone_checkboxes"/>
    <spring:url value="/js/star-rating.min.js" var="js_url_star_rarting"/>

    <!--<spring:url value="/js/profilPage.js" var="js_url_profil_page"/>-->

    <!--<script type="text/javascript" src="${js_url_profil_page}"><jsp:text/></script>-->
    <script type="text/javascript" src="${js_url_validation}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_jquery}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_bootstrap}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_bootstrap_datepicker}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_bootstrap_datepicker_locale}" charset="UTF-8">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_carmodelchoix}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_googlemap}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_moment_js}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_datetimepicker}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_bootstrap_multiselect}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_iphone_checkboxes}">
        <jsp:text/>
    </script>
    <script type="text/javascript" src="${js_url_star_rarting}">
        <jsp:text/>
    </script>


</head>
<body>
<spring:url value="/logout" var="url_logout"/>
<spring:url value="/login" var="url_login"/>
<spring:url value="/user/form" var="url_register"/>
<spring:url value="/user/account" var="url_profil_page"/>
<spring:url value="/user/preference/form" var="url_preference"/>

<c:if test="${pageContext.request.remoteUser != null}"><jsp:useBean id="_csrf" scope="request" type="org.hibernate.validator.internal.engine.ValueContext"/>

    <c:set var="loggedIn" value="true"/>

    <form id="logout-form" action="${url_logout}" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</c:if>

<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-inner">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <spring:url value="/" var="url_home"/>
            <a class="navbar-brand logo" href="${url_home}"><spring:message code="space"/></a>

            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${loggedIn}">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-expanded="false"><i class="fa fa-user"><spring:message
                                code="space"/>${pageContext.request.remoteUser}</i><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="${url_profil_page}"><spring:message code="user.account"/></a></li>
                                <li><a href="${url_preference}"><spring:message code="user.preference"/></a></li>
                                <li><a href="javascript:"
                                       onclick="document.getElementById('logout-form').submit();"><spring:message
                                        code="user.logout"/></a></li>
                            </ul>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-expanded="false"><i class="fa fa-user"><spring:message
                                code="space"/><spring:message code="user.guest"/></i><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="${url_login}"><spring:message code="user.login"/></a></li>
                                <li><a href="${url_register}"><spring:message code="user.register"/></a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"><i class="fa fa-flag"><spring:message
                            code="space"/></i><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="?lang=en_US">English</a></li>
                        <li><a href="?lang=fr_FR">Français</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<tiles:insertAttribute name="body"/>

<div id="map-canvas" class="col-sm-6 col-sm-offset-3"></div>
<div id="loading" class="modal"></div>

</body>
</html>