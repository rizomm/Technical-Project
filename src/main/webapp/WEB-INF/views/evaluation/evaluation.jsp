<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <title>Rate your Driver</title>
</head>

<style>
    #container1 {
        /*margin-bottom: 120px;*/
        padding: 20px;
    }

    .rating {
        margin-left: 30px;
    }

    div.skill {
        background: #5cb85c;
        border-radius: 3px;
        color: white;
        font-weight: bold;
        padding: 3px 4px;
        width: 100px;
        line-height: 2.5em;
    }

    .skillLine {
        display: inline-block;
        width: 100%;
        min-height: 90px;
        padding: 3px 4px;
    }

    .skillLineDefault {
        padding: 3px 4px;
    }
</style>
<body>
<div class="container">
    <div class="container" id="container1">
        <center>
            <h1 class="maintitle">Evaluation</h1>
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <form:form action="${baseURL}/mark/requestEvaluationDriver/submitEvaluationForm/${booking.id}" commandName="mark" method="post">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-center">Skills</h4>
                    </div>
                    <div class="panel-body text-center">
                        <p class="lead">
                            <strong>Rate your Journey !</strong>
                        </p>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item">
                            <div class="skillLineDefault">
                                <div class="skill pull-left text-center">Driving</div>
                                <!--<div class="rating" id="rate1"></div>-->
                                <form:input id="input-id" type="number" class="rating" min='0' max='5' step='1'
                                            data-size="lg" path="driverSkillMark"/>
                            </div>
                        </li>
                        <li class="list-group-item text-center">
                            <div class="skillLineDefault">
                                <div class="skill pull-left text-center">Amability</div>
                                <form:input id="input-id" type="number" class="rating" min='0' max='5' step='1'
                                       data-size="lg" path="amabilityMark"/>
                            </div>
                        </li>
                        <li class="list-group-item text-center">
                            <div class="skillLineDefault">
                                <div class="skill pull-left text-center">Punctuality</div>
                                <form:input id="input-id" type="number" class="rating" min='0' max='5' step='1'
                                       data-size="lg" path="ponctualityMark"/>
                            </div>
                        </li>
                    </ul>
                    <div class="panel-footer text-center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </form:form>
        </center>
    </div>
</div>
</body>
</html>