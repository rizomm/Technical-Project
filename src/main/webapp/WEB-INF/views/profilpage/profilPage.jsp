<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: ecockenp
  Date: 26/03/2015
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<spring:url value="/css/accountStyle.css" var="css_url_profile"/>


<spring:url value="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
            var="url_font_awesome_stylesheet"/>
<spring:url value="../css/star-rating.min.css" var="url_rating_stylesheet"/>
<link href="${url_font_awesome_stylesheet}" rel="stylesheet" type="text/css"/>
<link href="${url_rating_stylesheet}" rel="stylesheet" type="text/css"/>


<link href="${css_url_profile}" rel="stylesheet" type="text/css"/>
<div class="container">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
    <h1 class="maintitle">Profil</h1>
    <div class="row user-menu-container square">
        <center>
            <div class="col-md-7 user-details">
                <div class="row coralbg white">
                    <div class="col-md-6 no-pad">
                        <div class="user-pad">
                            <h3>${userVisited.firstname} ${userVisited.lastname}</h3>
                            <h4 class="white"><i class="fa fa-check-circle-o">

                            </i> ${userVisited.adresscity},<spring:message code="country.french"/></h4>
                            <h4 class="white">
                            	<c:if test="${userVisited.mark != null}">
                                	<div class="jRateProfil" data-rate="${userVisited.mark}"></div>
                                </c:if>
                            	<c:if test="${userVisited.mark == null}">
                                	<div class="jRateProfil" data-rate="0"></div>
                                </c:if>                             
                            </h4>
                        </div>
                        <div class="affinity"> Votre affinité: <span>${affinity}</span>%
                        </div>
                    </div>
                    <div class="col-md-6 no-pad">
                        <div class="">
                            <img src="https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg"
                                 class="img-responsive thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>
<script>
    $(document).ready(function () {
        var $btnSets = $('#responsive'),
                $btnLinks = $btnSets.find('a');

        $btnLinks.click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.user-menu>div.user-menu-content").removeClass("active");
            $("div.user-menu>div.user-menu-content").eq(index).addClass("active");
        });
    });

    $(document).ready(function () {
        $("[rel='tooltip']").tooltip();

        $('.view').hover(
                function () {
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function () {
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
        );
    });
</script>