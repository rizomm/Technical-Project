<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: ecockenp
  Date: 30/03/2015
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<s:url value="/js/evaluationPage.js" var="js_url_evaluationPage"/>
<script type="text/javascript" src="${js_url_evaluationPage}">
    <jsp:text/>
</script>

<s:url value="/css/font-awesome.min.css" var="css_url_fontawesome"/>
<link href="${css_url_fontawesome}" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    $(function () {
        $(document).foundation();
    });
</script>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<s:url value="${baseURL}/profil/profilePage" var="url_view_profile" />

<s:url value="/images/user_icons/music.png" var="url_icon_music" />
<s:url value="/images/user_icons/no_music.png" var="url_icon_no_music" />
<s:url value="/images/user_icons/smoker.png" var="url_icon_smoker" />
<s:url value="/images/user_icons/no_smoker.png" var="url_icon_no_smoker" />
<s:url value="/images/user_icons/talker.png" var="url_icon_talker" />
<s:url value="/images/user_icons/no_talker.png" var="url_icon_no_talker" />

<div class="row" id="container">
    <fieldset class="small-12 medium-12 large-12 bordure">
        <div class="small-12 medium-12 large-12 columns">
            <s:message code="space"/>
        </div>

        <ul class="accordion" data-accordion>

            <li class="accordion-navigation">
                <a href="#panel2a"><h2><s:message code="history.nextTrips"/></h2></a>

                <div id="panel2a" class="content active">
                    <table class="table table-striped small-12">
                        <thead>
                        <tr>
                            <th><s:message code="trip.start"/></th>
                            <th><s:message code="trip.end"/></th>
                            <th><s:message code="trip.startdate"/></th>
                            <th><s:message code="trip.enddate"/></th>
                            <th><s:message code="trip.numberofseat"/></th>
                            <th><s:message code="trip.cost"/></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="futureBooking" items="${futureBookingList}">
                            <tr>
                                <td>${futureBooking.addressStart}</td>
                                <td>${futureBooking.addressEnd}</td>
                                <td><fmt:formatDate value="${futureBooking.startdate}" pattern="dd/MM/yyyy HH:mm"/></td>
                                <td><fmt:formatDate value="${futureBooking.enddate}" pattern="dd/MM/yyyy HH:mm"/></td>
                                <td>${futureBooking.numberofseat}</td>
                                <td>${futureBooking.cost} €</td>
                                <s:url var="url_delete" value="/booking/delete/${futureBooking.id}"/>
                                <td><a class="button alert" href="${url_delete}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </li>

            <li class="accordion-navigation">
                <a href="#panel1a"><h2><s:message code="history.previousTrips"/></h2></a>

                <div id="panel1a" class="content">
                    <table class="table table-striped small-12">
                        <thead>
                        <tr>
                            <th><s:message code="trip.driver"/></th>
                            <th><s:message code="booking.trip"/></th>
                            <th><s:message code="trip.startdate"/></th>
                            <th><s:message code="trip.enddate"/></th>
                            <th><s:message code="trip.numberofseat"/></th>
                            <th><s:message code="trip.cost"/></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${bookingUserMap}" var="bookingUserMap">
                            <tr>

                                <td>
                                    <p>
                                        <a data-reveal-id="modalProfil" href="#" data-reveal-ajax="${url_view_profile}/${bookingUserMap.value.id}">${bookingUserMap.value.firstname} ${bookingUserMap.value.lastname}</a>
                                    </p>

                                    <p>${bookingUserMap.value.username}</p>

                                    <p>

                                    <div class="jRateHistory" data-rate="${bookingUserMap.value.mark}"></div>
                                    </p>
                                    <p>
                                    <ul class="inline-list">
                                        <li>
                                            <c:choose>
                                                <c:when test="${bookingUserMap.value.talker == true}">
                                                    <img src="${url_icon_talker}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img src="${url_icon_no_talker}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                        <li>
                                            <c:choose>
                                                <c:when test="${bookingUserMap.value.listenmusic == true}">
                                                    <img src="${url_icon_music}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img src="${url_icon_no_music}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                        <li>
                                            <c:choose>
                                                <c:when test="${bookingUserMap.value.smoker == true}">
                                                    <img src="${url_icon_smoker}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img src="${url_icon_no_smoker}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                    </ul>
                                    </p>

                                </td>
                                <td>${bookingUserMap.key.addressStart} <s:message
                                        code="to"/> ${bookingUserMap.key.addressEnd}</td>
                                <td><fmt:formatDate value="${bookingUserMap.key.startdate}"
                                                    pattern="dd/MM/yyyy HH:mm"/></td>
                                <td><fmt:formatDate value="${bookingUserMap.key.enddate}"
                                                    pattern="dd/MM/yyyy HH:mm"/></td>
                                <td>${bookingUserMap.key.numberofseat}</td>
                                <td>${bookingUserMap.key.cost} €</td>
                                <td>
                                    <c:if test="${!bookingUserMap.key.evaluated}">
                                        <button data-reveal-id="modalEvaluate" data-reveal-ajax="${baseURL}/mark/requestEvaluationDriver/${bookingUserMap.key.id}">
                                            <s:message code="eval"/>
                                        </button>
                                    </c:if>
                                </td>
                            </tr>

                            <div id="modalEvaluate" class="reveal-modal" data-reveal>
                                <h1 class="maintitle">Evaluation</h1>
                            </div>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </li>

        </ul>

        <div id="modalProfil" class="reveal-modal" data-reveal="true">
            <h1 class="maintitle">Profil</h1>
        </div>
    </fieldset>
</div>	
