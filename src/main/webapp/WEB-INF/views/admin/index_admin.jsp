<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<c:set var="req" value="${pageContext.request}" />
	<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	
<body>
	<spring:url value="/logout" var="url_logout" />

       <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Admin Dashboard</h2>   
                        <h5>Welcome ${user.firstname} ${user.lastname}</h5>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"> ${nbUsers} New</p>
                    <p class="text-muted">Users</p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-map-marker"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"> ${nbBookings} New</p>
                    <p class="text-muted">Bookings</p>
                </div>
             </div>
		     </div>
            <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-road"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"> ${nbTrips} New</p>
                    <p class="text-muted">Trips</p>
                </div>
             </div>
		     </div>
			 <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-dashboard"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">${kmTraveled} Km</p>
                    <p class="text-muted">Traveled</p>
                </div>
             </div>
		     </div>

			</div>
                 <!-- /. ROW  -->
                <hr />                

                 <!-- /. ROW  -->
                <div class="row"> 
                    
                      
                <div class="col-md-9 col-sm-12 col-xs-12">                     
                    <div class="panel panel-default">
         
                        <div class="panel-body">
 <div class="row" >

                   
               
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Last Bookings
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Distance</th>
                                            <th>Cost</th>
											<th>Departure</th>
											<th>Arrival</th>
											<th>Start date</th>
											<th>End date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<c:forEach items="${lastBookings}" var="lastbooking">
	                                        <tr>
	                                            <td>${lastbooking.distance} Km</td>
	                                            <td>${lastbooking.cost} &euro;</td>
	                                            <td>${lastbooking.addressStart}</td>
	                                            <td>${lastbooking.addressEnd}</td>
												<td>${lastbooking.startdate}</td>
												<td>${lastbooking.enddate}</td>
	                                        </tr>
	                                    </c:forEach>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                  
                </div>
                        </div>
                    </div>            
                </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">                       
                    <div class="panel panel-primary text-center no-boder bg-color-green">
                        <div class="panel-body">
                            <i class="fa fa-star-o fa-5x"></i>
                            <h3>${goodEvaluatedDrivers} drivers </h3>
                        </div>
                        <div class="panel-footer back-footer-green">
                           Good evaluated
                            
                        </div>
                    </div>
                    <div class="panel panel-primary text-center no-boder bg-color-red">
                        <div class="panel-body">
                            <i class="fa fa-arrow-up fa-5x"></i>
                            <h3>${nbDrivers} passengers</h3>
                        </div>
                        <div class="panel-footer back-footer-red">
                            Become drivers  
                        </div>
                    </div>                         
                        </div>
                
           </div>
                 <!-- /. ROW  -->   
                 <!-- /. ROW  -->           
    </div>
             <!-- /. PAGE INNER  -->
            </div>
        

	

	
</body>