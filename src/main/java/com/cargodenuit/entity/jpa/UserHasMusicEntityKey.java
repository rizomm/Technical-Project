/*
 * Created on 27 f�vr. 2015 ( Time 17:36:22 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.entity.jpa;
import java.io.Serializable;

import javax.persistence.*;

/**
 * Composite primary key for entity "UserHasMusicEntity" ( stored in table "user_has_music" )
 *
 * @author Telosys Tools Generator
 *
 */
 @Embeddable
public class UserHasMusicEntityKey implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY KEY ATTRIBUTES 
    //----------------------------------------------------------------------
    @Column(name="user_id", nullable=false)
    private Integer    userId       ;
    
    @Column(name="music_id", nullable=false)
    private Integer    musicId      ;
    

    //----------------------------------------------------------------------
    // CONSTRUCTORS
    //----------------------------------------------------------------------
    public UserHasMusicEntityKey() {
        super();
    }

    public UserHasMusicEntityKey( Integer userId, Integer musicId ) {
        super();
        this.userId = userId ;
        this.musicId = musicId ;
    }
    
    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR KEY FIELDS
    //----------------------------------------------------------------------
    public void setUserId( Integer value ) {
        this.userId = value;
    }
    public Integer getUserId() {
        return this.userId;
    }

    public void setMusicId( Integer value ) {
        this.musicId = value;
    }
    public Integer getMusicId() {
        return this.musicId;
    }


    //----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		UserHasMusicEntityKey other = (UserHasMusicEntityKey) obj; 
		//--- Attribute userId
		if ( userId == null ) { 
			if ( other.userId != null ) 
				return false ; 
		} else if ( ! userId.equals(other.userId) ) 
			return false ; 
		//--- Attribute musicId
		if ( musicId == null ) { 
			if ( other.musicId != null ) 
				return false ; 
		} else if ( ! musicId.equals(other.musicId) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // hashCode METHOD
    //----------------------------------------------------------------------
	public int hashCode() { 
		final int prime = 31; 
		int result = 1; 
		
		//--- Attribute userId
		result = prime * result + ((userId == null) ? 0 : userId.hashCode() ) ; 
		//--- Attribute musicId
		result = prime * result + ((musicId == null) ? 0 : musicId.hashCode() ) ; 
		
		return result; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(userId); 
		sb.append("|"); 
		sb.append(musicId); 
        return sb.toString();
    }
}
