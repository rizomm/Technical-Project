/*
 * Created on 27 f�vr. 2015 ( Time 17:36:22 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
// This Bean has a composite Primary Key  


package com.cargodenuit.entity.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "user_has_interest"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="user_has_interest", catalog="cargodenuit" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="UserHasInterestEntity.countAll", query="SELECT COUNT(x) FROM UserHasInterestEntity x" )
} )
public class UserHasInterestEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( EMBEDDED IN AN EXTERNAL CLASS )  
    //----------------------------------------------------------------------
	@EmbeddedId
    private UserHasInterestEntityKey compositePrimaryKey ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public UserHasInterestEntity() {
		super();
		this.compositePrimaryKey = new UserHasInterestEntityKey();       
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE COMPOSITE KEY 
    //----------------------------------------------------------------------
    public void setUserId( Integer userId ) {
        this.compositePrimaryKey.setUserId( userId ) ;
    }
    public Integer getUserId() {
        return this.compositePrimaryKey.getUserId() ;
    }
    public void setInterestId( Integer interestId ) {
        this.compositePrimaryKey.setInterestId( interestId ) ;
    }
    public Integer getInterestId() {
        return this.compositePrimaryKey.getInterestId() ;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        if ( compositePrimaryKey != null ) {  
            sb.append(compositePrimaryKey.toString());  
        }  
        else {  
            sb.append( "(null-key)" ); 
        }  
        sb.append("]:"); 
        return sb.toString(); 
    } 

}
