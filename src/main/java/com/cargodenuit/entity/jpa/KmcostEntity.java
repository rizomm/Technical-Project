/*
 * Created on 27 f�vr. 2015 ( Time 17:36:22 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.cargodenuit.entity.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.math.BigDecimal;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "kmcost"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="kmcost", catalog="cargodenuit" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="KmcostEntity.countAll", query="SELECT COUNT(x) FROM KmcostEntity x" )
} )
public class KmcostEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")  
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="cost", nullable=false)
    private BigDecimal cost         ;

	// "fuelId" (column "fuel_id") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="fuel_id", referencedColumnName="id")
    private FuelEntity fuel        ;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public KmcostEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : cost ( DECIMAL ) 
    public void setCost( BigDecimal cost ) {
        this.cost = cost;
    }
    public BigDecimal getCost() {
        return this.cost;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setFuel( FuelEntity fuel ) {
        this.fuel = fuel;
    }
    public FuelEntity getFuel() {
        return this.fuel;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(cost);
        return sb.toString(); 
    } 

}
