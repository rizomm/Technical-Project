package com.cargodenuit.entity;

import javax.validation.constraints.NotNull;

/**
 * Created by ecockenp on 03/04/2015 for Technical-Project.
 */
public class Mark {

    @NotNull
    private int amabilityMark;
    @NotNull
    private int driverSkillMark;
    @NotNull
    private int ponctualityMark;

    public Mark() {
    }

    public int getAmabilityMark() {
        return amabilityMark;
    }

    public void setAmabilityMark( int amabilityMark ) {
        this.amabilityMark = amabilityMark;
    }

    public int getDriverSkillMark() {
        return driverSkillMark;
    }

    public void setDriverSkillMark( int driverSkillMark ) {
        this.driverSkillMark = driverSkillMark;
    }

    public int getPonctualityMark() {
        return ponctualityMark;
    }

    public void setPonctualityMark( int ponctualityMark ) {
        this.ponctualityMark = ponctualityMark;
    }
}
