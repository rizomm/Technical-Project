/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.business.service.impl;

import com.cargodenuit.business.service.UserRolesService;
import com.cargodenuit.business.service.mapping.UserRolesServiceMapper;
import com.cargodenuit.data.repository.jpa.UserRolesJpaRepository;
import com.cargodenuit.entity.UserRoles;
import com.cargodenuit.entity.jpa.UserRolesEntity;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of UserService
 */
@Component
@Transactional
public class UserRolesServiceImpl implements UserRolesService {

	@Resource
	private UserRolesJpaRepository userRolesJpaRepository;

	@Resource
	private UserRolesServiceMapper userRolesServiceMapper;
	
	@Override
	public UserRoles findById(Integer id) {
		UserRolesEntity userRolesEntity = userRolesJpaRepository.findOne(id);
		return userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntity);
	}
	
	@Override
	public UserRoles findByRole(String role) {
		UserRolesEntity userRolesEntity = userRolesJpaRepository.findByRole(role);
		return userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntity);
	}
	
	@Override
	public List<UserRoles> findByUsername(String email) {
		Iterable<UserRolesEntity> entities = userRolesJpaRepository.findByUsername(email);
		List<UserRoles> beans = new ArrayList<UserRoles>();
		for(UserRolesEntity userRolesEntity : entities) {
			beans.add(userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntity));
		}
		return beans;
	}

	@Override
	public List<UserRoles> findAll() {
		Iterable<UserRolesEntity> entities = userRolesJpaRepository.findAll(sortByIdAsc());
		List<UserRoles> beans = new ArrayList<UserRoles>();
		for(UserRolesEntity userRolesEntity : entities) {
			beans.add(userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntity));
		}
		
		return beans;
	}
	
    private Sort sortByIdAsc() {
        return new Sort(Sort.Direction.ASC, "id");
    }

	@Override
	public UserRoles save(UserRoles userRoles) {
		return update(userRoles) ;
	}

	@Override
	public UserRoles create(UserRoles userRoles) {
		UserRolesEntity userRolesEntity = userRolesJpaRepository.findOne(userRoles.getId());
		if( userRolesEntity != null ) {
			throw new IllegalStateException("already.exists");
		}
		userRolesEntity = new UserRolesEntity();
		
		userRolesServiceMapper.mapUserRolesToUserRolesEntity(userRoles, userRolesEntity);
		UserRolesEntity userRolesEntitySaved = userRolesJpaRepository.save(userRolesEntity);
		return userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntitySaved);
	}

	@Override
	public UserRoles update(UserRoles userRoles) {
		UserRolesEntity userRolesEntity = userRolesJpaRepository.findOne(userRoles.getId());
		userRolesServiceMapper.mapUserRolesToUserRolesEntity(userRoles, userRolesEntity);
		UserRolesEntity userRolesEntitySaved = userRolesJpaRepository.save(userRolesEntity);
		return userRolesServiceMapper.mapUserRolesEntityToUserRoles(userRolesEntitySaved);
	}

	@Override
	public void delete(Integer id) {
		userRolesJpaRepository.delete(id);
	}

	public UserRolesJpaRepository getUserRolesJpaRepository() {
		return userRolesJpaRepository;
	}

	public void setUserRolesJpaRepository(UserRolesJpaRepository userRolesJpaRepository) {
		this.userRolesJpaRepository = userRolesJpaRepository;
	}

	public UserRolesServiceMapper getUserRolesServiceMapper() {
		return userRolesServiceMapper;
	}

	public void setUserRolesServiceMapper(UserRolesServiceMapper userRolesServiceMapper) {
		this.userRolesServiceMapper = userRolesServiceMapper;
	}
	
	@Override
	public Boolean isAdmin(String username) {
		Iterable<UserRolesEntity> entities = userRolesJpaRepository.findByUsername(username);
		Boolean isAdmin = false ;
		for(UserRolesEntity userRolesEntity : entities) {
			if(userRolesEntity.getRole() == "ROLE_ADMIN"){
				isAdmin = true ;
			}
		}
		
		return isAdmin;
	}

}
