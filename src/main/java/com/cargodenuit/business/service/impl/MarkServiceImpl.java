package com.cargodenuit.business.service.impl;

import com.cargodenuit.business.service.MarkService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ecockenp on 03/04/2015 for Technical-Project.
 */
@Component
@Transactional
public class MarkServiceImpl implements MarkService {

    @Override
    public float averageMarkDriverByBooking( int amabilityMark, int driverSkillMark, int punctualityMark ) {

        return (float) (( amabilityMark + driverSkillMark + punctualityMark ) / 3);
    }

    @Override
    public float averageMarkByDriver( float averageMarkDriverByBooking, float averageMark ) {

        return ( averageMarkDriverByBooking + averageMark ) / 2;
    }
}
