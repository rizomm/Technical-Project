/*
 * Created on 27 f�vr. 2015 ( Time 17:36:31 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import com.cargodenuit.entity.Interest;
import com.cargodenuit.entity.jpa.InterestEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class InterestServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public InterestServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'InterestEntity' to 'Interest'
	 * @param interestEntity
	 */
	public Interest mapInterestEntityToInterest(InterestEntity interestEntity) {
		if(interestEntity == null) {
			return null;
		}

		//--- Generic mapping 
		Interest interest = map(interestEntity, Interest.class);

		return interest;
	}
	
	/**
	 * Mapping from 'Interest' to 'InterestEntity'
	 * @param interest
	 * @param interestEntity
	 */
	public void mapInterestToInterestEntity(Interest interest, InterestEntity interestEntity) {
		if(interest == null) {
			return;
		}

		//--- Generic mapping 
		map(interest, interestEntity);

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}