/*
 * Created on 27 f�vr. 2015 ( Time 17:36:31 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import com.cargodenuit.entity.Car;
import com.cargodenuit.entity.jpa.CarEntity;
import com.cargodenuit.entity.jpa.CarmodelEntity;
import com.cargodenuit.entity.jpa.FuelEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class CarServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public CarServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'CarEntity' to 'Car'
	 * @param carEntity
	 */
	public Car mapCarEntityToCar(CarEntity carEntity) {
		if(carEntity == null) {
			return null;
		}

		//--- Generic mapping 
		Car car = map(carEntity, Car.class);

		//--- Link mapping ( link to Carmodel )
		if(carEntity.getCarmodel() != null) {
			car.setCarmodelId(carEntity.getCarmodel().getId());
		}
		//--- Link mapping ( link to Fuel )
		if(carEntity.getFuel() != null) {
			car.setFuelId(carEntity.getFuel().getId());
		}
		return car;
	}
	
	/**
	 * Mapping from 'Car' to 'CarEntity'
	 * @param car
	 * @param carEntity
	 */
	public void mapCarToCarEntity(Car car, CarEntity carEntity) {
		if(car == null) {
			return;
		}

		//--- Generic mapping 
		map(car, carEntity);

		//--- Link mapping ( link : car )
		if( hasLinkToCarmodel(car) ) {
			CarmodelEntity carmodel1 = new CarmodelEntity();
			carmodel1.setId( car.getCarmodelId() );
			carEntity.setCarmodel( carmodel1 );
		} else {
			carEntity.setCarmodel( null );
		}

		//--- Link mapping ( link : car )
		if( hasLinkToFuel(car) ) {
			FuelEntity fuel2 = new FuelEntity();
			fuel2.setId( car.getFuelId() );
			carEntity.setFuel( fuel2 );
		} else {
			carEntity.setFuel( null );
		}

	}
	
	/**
	 * Verify that Carmodel id is valid.
	 * @param Carmodel Carmodel
	 * @return boolean
	 */
	private boolean hasLinkToCarmodel(Car car) {
		return car.getCarmodelId() != null;
	}

	/**
	 * Verify that Fuel id is valid.
	 * @param Fuel Fuel
	 * @return boolean
	 */
	private boolean hasLinkToFuel(Car car) {
		return car.getFuelId() != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}