package com.cargodenuit.business.service;

/**
 * Created by ecockenp on 03/04/2015 for Technical-Project.
 */
public interface MarkService {

    /**
     * Service that allow to calculate the average driver mark for booking , marked by passenger .
     *
     * @param amabilityMark the amability mark
     * @param driverSkillMark the driver skill mark
     * @param punctualityMark the punctuallity Mark.
     * @return the average mark for a driver for booking
     */
    float averageMarkDriverByBooking( int amabilityMark, int driverSkillMark, int punctualityMark );

    /**
     *  Service that calculate the average driver mark
     * @param averageMarkDriverByBooking the averageMarkDriverByBooking
     * @param averageMark the current average driver mark
     * @return the calculated average mark for the driver.
     */
    float averageMarkByDriver( float averageMarkDriverByBooking, float averageMark );

}
