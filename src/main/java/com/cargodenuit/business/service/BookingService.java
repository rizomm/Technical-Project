/*
 * Created on 27 f�vr. 2015 ( Time 17:36:30 )
 */
package com.cargodenuit.business.service;

import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.User;

import java.util.List;

import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.Trip;

/**
 * Business Service Interface for entity Booking.
 */
public interface BookingService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param id
	 * @return entity
	 */
	Booking findById( Integer id  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<Booking> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	Booking save(Booking entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	Booking update(Booking entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	Booking create(Booking entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param id
	 */
	void delete( Integer id );
	
    /**
     * Permit to find a booking thanks to the user id.
     *
     * @param UserId the User id
     * @return the list of booking by user ID
     */
    List<Booking> findByUserId( Integer UserId );

    /**
     * Allow to find the list of user 's trip  if a trip is past
     *
     * @return List  of past trips
     */
    List<Booking> pastBookingByUserId( Integer id );

    /**
     * Allow to find the list of user 's trip  if a trip is future
     *
     * @return List  of future trips
     */
    List<Booking> futureBookingByUserId( Integer id );

    /**
     * Permit to return the passenger who made the booking
     *
     * @param booking is the actual booking made by passenger
     * @return a User.
     */
    User getPassenger( Booking booking );

}
