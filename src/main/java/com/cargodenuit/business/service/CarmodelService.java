/*
 * Created on 27 f�vr. 2015 ( Time 17:36:31 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.business.service;

import java.util.List;

import com.cargodenuit.entity.Brand;
import com.cargodenuit.entity.Carmodel;
import com.cargodenuit.entity.jpa.BrandEntity;
import com.cargodenuit.entity.jpa.CarmodelEntity;

/**
 * Business Service Interface for entity Carmodel.
 */
public interface CarmodelService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param id
	 * @return entity
	 */
	Carmodel findById( Integer id  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<Carmodel> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	Carmodel save(Carmodel entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	Carmodel update(Carmodel entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	Carmodel create(Carmodel entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param id
	 */
	void delete( Integer id );
	
	/**
	 * Retourne tous les modèles correspondant à une marque
	 * 
	 */
	List<Carmodel> findByBrand(BrandEntity brandid);

	Carmodel findCarmodelByCarId(Integer id);
	


}
