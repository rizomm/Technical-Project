package com.cargodenuit.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	  	  auth.jdbcAuthentication().dataSource(dataSource)
	  	  	.passwordEncoder(passwordEncoder())
	  		.usersByUsernameQuery(
	  			"select username,password, enabled from user where username=?")
	  		.authoritiesByUsernameQuery(
	  			"select username, role from user_roles where username=?");
	 }
    
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	   @Bean(name="authenticationManager")
	   @Override
	   public AuthenticationManager authenticationManagerBean() throws Exception {
	       return super.authenticationManagerBean();
	   }
  
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            	//ALL
                .antMatchers("/").permitAll() 
                .antMatchers("/login").permitAll() 
                
                // MEMBER
                .antMatchers("user/preference/form").authenticated()
                .antMatchers("user/preference/form/").authenticated()
                .antMatchers("user/form/account/").authenticated()
                .antMatchers("user/form/account").authenticated()
                .antMatchers("/user/driver").hasRole("PASSENGER")
                .antMatchers("/car/form").hasRole("PASSENGER")
                .antMatchers("/booking").hasRole("PASSENGER")
                .antMatchers("/booking/*").hasRole("PASSENGER")
                
                //DRIVER
                .antMatchers("/trip/form").hasRole("DRIVER")
                .antMatchers("/trip").hasRole("DRIVER")
                .antMatchers("/car/form/").hasRole("DRIVER")
                .antMatchers("/car/form").hasRole("DRIVER")
                
                //ADMIN
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/admin/*").hasRole("ADMIN")
                .antMatchers("/fuel/*").hasRole("ADMIN")
                .antMatchers("/fuel").hasRole("ADMIN")
                .antMatchers("/carmodel/*").hasRole("ADMIN")
                .antMatchers("/carmodel").hasRole("ADMIN")
                .antMatchers("/brand/*").hasRole("ADMIN")
                .antMatchers("/brand").hasRole("ADMIN")
                .antMatchers("/music/*").hasRole("ADMIN")
                .antMatchers("/music").hasRole("ADMIN")
                .antMatchers("/interest/*").hasRole("ADMIN")
                .antMatchers("/interest").hasRole("ADMIN")
                .antMatchers("/user/").hasRole("ADMIN")
                .antMatchers("/user").hasRole("ADMIN")    
                .antMatchers("/userHasInterest").hasRole("ADMIN")  
                .antMatchers("/userHasInterest/*").hasRole("ADMIN")  
                .antMatchers("/userHasMusic").hasRole("ADMIN")  
                .antMatchers("/userHasMusic/*").hasRole("ADMIN")  
                .antMatchers("/kmcost").hasRole("ADMIN")  
                .antMatchers("/kmcost/*").hasRole("ADMIN")   
                .antMatchers("/car/").hasRole("ADMIN")   
                .antMatchers("/car").hasRole("ADMIN")  
                .and()
            .formLogin()
            	.loginPage("/login")
                .permitAll()
                .and()
            .logout()      
	            .logoutSuccessUrl("/")
	            .deleteCookies("JSESSIONID")
                .permitAll()
           .and()
           		.exceptionHandling().accessDeniedPage("/403");      
    }
    

    
}


/*
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
	@Autowired
	DataSource dataSource;
 
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
 
	  auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery(
			"select username,password, enabled from user where username=?")
		.authoritiesByUsernameQuery(
			"select username, role from user_role where username=?");
	}	
 
	@Override
	protected void configure(HttpSecurity http) throws Exception {
 
	  http.authorizeRequests()
		.antMatchers("/product/basket/").access("hasRole('ROLE_USER')")
		  .and().formLogin().loginPage("/login").failureUrl("/login?error")
		  .usernameParameter("username").passwordParameter("password")
		.and()
		  .logout().logoutSuccessUrl("/login?logout")
		.and()
		  .exceptionHandling().accessDeniedPage("/403")
		.and()
		  .csrf();
	  
	}
}

*/