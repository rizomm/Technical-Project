/*
 * Created on 27 f�vr. 2015 ( Time 17:36:14 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.rest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.cargodenuit.entity.Kmcost;
import com.cargodenuit.business.service.KmcostService;
import com.cargodenuit.web.listitem.KmcostListItem;

/**
 * Spring MVC controller for 'Kmcost' management.
 */
@Controller
public class KmcostRestController {

	@Resource
	private KmcostService kmcostService;
	
	@RequestMapping( value="/items/kmcost",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<KmcostListItem> findAllAsListItems() {
		List<Kmcost> list = kmcostService.findAll();
		List<KmcostListItem> items = new LinkedList<KmcostListItem>();
		for ( Kmcost kmcost : list ) {
			items.add(new KmcostListItem( kmcost ) );
		}
		return items;
	}
	
	@RequestMapping( value="/kmcost",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Kmcost> findAll() {
		return kmcostService.findAll();
	}

	@RequestMapping( value="/kmcost/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Kmcost findOne(@PathVariable("id") Integer id) {
		return kmcostService.findById(id);
	}
	
	@RequestMapping( value="/kmcost",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Kmcost create(@RequestBody Kmcost kmcost) {
		return kmcostService.create(kmcost);
	}

	@RequestMapping( value="/kmcost/{id}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Kmcost update(@PathVariable("id") Integer id, @RequestBody Kmcost kmcost) {
		return kmcostService.update(kmcost);
	}

	@RequestMapping( value="/kmcost/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		kmcostService.delete(id);
	}
	
}
