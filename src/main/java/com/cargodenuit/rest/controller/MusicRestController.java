/*
 * Created on 27 f�vr. 2015 ( Time 17:36:14 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.rest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.cargodenuit.entity.Music;
import com.cargodenuit.business.service.MusicService;
import com.cargodenuit.web.listitem.MusicListItem;

/**
 * Spring MVC controller for 'Music' management.
 */
@Controller
public class MusicRestController {

	@Resource
	private MusicService musicService;
	
	@RequestMapping( value="/items/music",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<MusicListItem> findAllAsListItems() {
		List<Music> list = musicService.findAll();
		List<MusicListItem> items = new LinkedList<MusicListItem>();
		for ( Music music : list ) {
			items.add(new MusicListItem( music ) );
		}
		return items;
	}
	
	@RequestMapping( value="/music",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Music> findAll() {
		return musicService.findAll();
	}

	@RequestMapping( value="/music/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Music findOne(@PathVariable("id") Integer id) {
		return musicService.findById(id);
	}
	
	@RequestMapping( value="/music",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Music create(@RequestBody Music music) {
		return musicService.create(music);
	}

	@RequestMapping( value="/music/{id}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Music update(@PathVariable("id") Integer id, @RequestBody Music music) {
		return musicService.update(music);
	}

	@RequestMapping( value="/music/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		musicService.delete(id);
	}
	
}
