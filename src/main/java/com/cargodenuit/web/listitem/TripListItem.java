/*
 * Created on 27 f�vr. 2015 ( Time 17:36:14 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.web.listitem;

import com.cargodenuit.entity.Trip;
import com.cargodenuit.web.common.ListItem;

public class TripListItem implements ListItem {

	private final String value ;
	private final String label ;
	
	public TripListItem(Trip trip) {
		super();

		this.value = ""
			 + trip.getId()
		;

		//TODO : Define here the attributes to be displayed as the label
		this.label = trip.toString();
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
