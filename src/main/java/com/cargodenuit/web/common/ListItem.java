/*
 * Created on 27 f�vr. 2015 ( Time 17:36:14 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.web.common;

public interface ListItem {

	String getValue();
	
	String getLabel();
}
