package com.cargodenuit.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cargodenuit.business.service.BookingService;
import com.cargodenuit.business.service.BrandService;
import com.cargodenuit.business.service.CarService;
import com.cargodenuit.business.service.CarmodelService;
import com.cargodenuit.business.service.FuelService;
import com.cargodenuit.business.service.UserRolesService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.Brand;
import com.cargodenuit.entity.Car;
import com.cargodenuit.entity.Fuel;
import com.cargodenuit.entity.Trip;
import com.cargodenuit.entity.User;
import com.cargodenuit.entity.UserRoles;
import com.cargodenuit.web.common.FormMode;
import com.cargodenuit.web.listitem.FuelListItem;
 
@Controller
public class IndexController {
	
	@Resource
    private UserRolesService userRolesService;
	
	@Resource
    private UserService userService;
	
	@Resource
    private BookingService bookingService;
	
	@Resource
    private CarService carService; 
	@Resource
    private CarmodelService carmodelService; 
	@Resource
    private FuelService fuelService;
	@Resource
    private BrandService brandService; 

	
	
	
	public User getLastUser(final Collection<User> c) {
	    final Iterator<User> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (User) lastElement;
	}
	
	public Car getLastCar(final Collection<Car> c) {
	    final Iterator<Car> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (Car) lastElement;
	}
	
	@RequestMapping(value = "")
	public ModelAndView index() {
		
		ModelAndView model = new ModelAndView("index");
		
		/**
		 *  On construit le form pour créer un user
		 */
		User user = new User();	
		
		List<User> allUsers  = userService.findAll();
		User lastuser		 = getLastUser(allUsers);
		
		if(allUsers != null && !allUsers.isEmpty()){
			user.setId(lastuser.getId()+1);
		}
		else{
			user.setId(0);
		}
		
 
		/**
		 * On construit le form pour créer une voiture
		 */
		Car car = new Car();	
		Integer idcar = 0 ;
		
		List<Car> allCars  = carService.findAll();
		
		if(allCars != null && !allCars.isEmpty()){
			Car lastCar		= getLastCar(allCars);
			idcar 			= lastCar.getId() +1 ;
			car.setId(idcar);
		}
		else{
			car.setId(idcar);
		}
				
		List<Brand> listofbrand = brandService.findAll();
		List<Fuel> list = fuelService.findAll();
		List<FuelListItem> listOfFuelItems = new LinkedList<FuelListItem>();
		for ( Fuel fuel : list ) {
			listOfFuelItems.add(new FuelListItem( fuel ) );
		}

		
		
		/**
		 * On construit la home page
		 */
		Boolean isLogged 			= false ;
		Boolean isDriver 			= false ;
		Boolean isPassenger 		= false ;
		Boolean isAdmin				= false ;
		
		List<Booking> bookingList 	= new ArrayList<Booking>() ;
	
		// On test si il y a un user connecté
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
	    
	    if(auth.getName() != "anonymousUser"){
	    	
	    	isLogged 				= true;
	    	 
	    	//On recupere la liste des roles du ser
	    	 List<UserRoles> userRoles 	= userRolesService.findByUsername(auth.getName());
	    	 User connectedUser 		= userService.findByUsername(auth.getName());
	    	 	    	 
	    	 //On boucle sur la liste des roles du user et si on trouve le ROLE_USER ou le ROLE_ADMIN, on met les flags a true
	    	 for( UserRoles role : userRoles ){
	    		 if(role.getRole().contentEquals("ROLE_DRIVER")){
	    			 isDriver		= true;  
	    		 }
	    		 if(role.getRole().contentEquals("ROLE_ADMIN")){
	    			 isAdmin		= true;  
	    		 }
	    		 if(role.getRole().contentEquals("ROLE_PASSENGER")){
	    			 isPassenger	= true;  
	    		 }	    		 
	    		 
	    	 }
	    	 
	    	 bookingList = bookingService.findByUserId(connectedUser.getId());
	    	
	    }
	   
	    
		model.addObject("isLogged", isLogged);
		model.addObject("isDriver", isDriver);
		model.addObject("isPassenger", isPassenger);
		model.addObject("isAdmin", isAdmin);
		model.addObject("bookingList", bookingList);
		model.addObject("user", user);
		model.addObject("car", car);
		model.addObject("listofbrand", listofbrand);
		model.addObject("listOfFuelItems", listOfFuelItems);
 
		return model;
 
	}
 
}