package com.cargodenuit.web.controller;
 

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
 

@Controller
public class LoginController {
 
	//Spring Security see this :
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(RedirectAttributes redirectAttributes,
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
		if (error != null) {
			redirectAttributes.addFlashAttribute("message", "Invalid username and password!");
			return "redirect:/";
		}
 
		if (logout != null) {
			redirectAttributes.addFlashAttribute("message", "You've been logged out successfully.");
		}
 
		return "redirect:/";
 
	}
	
 
}