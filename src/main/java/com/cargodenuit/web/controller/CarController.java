package com.cargodenuit.web.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

























//--- Common classes
import com.cargodenuit.web.common.AbstractController;
import com.cargodenuit.web.common.FormMode;
import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageType;
import com.cargodenuit.entity.Brand;
//--- Entities
import com.cargodenuit.entity.Car;
import com.cargodenuit.entity.Carmodel;
import com.cargodenuit.entity.Fuel;
import com.cargodenuit.entity.User;
import com.cargodenuit.entity.UserRoles;
import com.cargodenuit.entity.jpa.BrandEntity;
import com.cargodenuit.entity.jpa.CarEntity;
import com.cargodenuit.business.service.BrandService;
//--- Services 
import com.cargodenuit.business.service.CarService;
import com.cargodenuit.business.service.CarmodelService;
import com.cargodenuit.business.service.FuelService;
import com.cargodenuit.business.service.UserRolesService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.business.service.mapping.CarServiceMapper;
//--- List Items 
import com.cargodenuit.web.listitem.CarmodelListItem;
import com.cargodenuit.web.listitem.FuelListItem;

/**
 * Spring MVC controller for 'Car' management.
 */
@Controller
@RequestMapping("/car")
public class CarController extends AbstractController {

	//--- Variables names ( to be used in JSP with Expression Language )
	private static final String MAIN_ENTITY_NAME = "car";
	private static final String MAIN_LIST_NAME   = "list";

	//--- JSP pages names ( View name in the MVC model )
	private static final String JSP_FORM   = "car/form";
	private static final String JSP_LIST   = "car/list";

	//--- SAVE ACTION ( in the HTML form )
	private static final String SAVE_ACTION_CREATE   = "/car/create";
	private static final String SAVE_ACTION_UPDATE   = "/car/update";

	//--- Main entity service
	@Resource
    private CarService carService; 
	//--- Other service(s)
	@Resource
    private CarmodelService carmodelService; 
	@Resource
    private FuelService fuelService;
	@Resource
    private BrandService brandService; 
	@Resource
    private CarServiceMapper carServiceMapper; 	
	@Resource
    private UserService userService;
	@Resource
    private UserRolesService userRolesService;
	

	//--------------------------------------------------------------------------------------
	/**
	 * Default constructor
	 */
	public CarController() {
		super(CarController.class, MAIN_ENTITY_NAME );
		log("CarController created.");
	}

	//--------------------------------------------------------------------------------------
	// Spring MVC model management
	//--------------------------------------------------------------------------------------
	/**
	 * Populates the combo-box "items" for the referenced entity "Carmodel"
	 * @param model
	 */
	private void populateListOfCarmodelItems(Model model) {
		List<Carmodel> list = carmodelService.findAll();
		List<CarmodelListItem> items = new LinkedList<CarmodelListItem>();
		for ( Carmodel carmodel : list ) {
			items.add(new CarmodelListItem( carmodel ) );
		}
		model.addAttribute("listOfCarmodelItems", items ) ;
	}

	/**
	 * Populates the combo-box "items" for the referenced entity "Fuel"
	 * @param model
	 */
	private void populateListOfFuelItems(Model model) {
		List<Fuel> list = fuelService.findAll();
		List<FuelListItem> items = new LinkedList<FuelListItem>();
		for ( Fuel fuel : list ) {
			items.add(new FuelListItem( fuel ) );
		}
		model.addAttribute("listOfFuelItems", items ) ;
	}


	/**
	 * Populates the Spring MVC model with the given entity and eventually other useful data
	 * @param model
	 * @param car
	 */
	private void populateModel(Model model, Car car, FormMode formMode) {
		//--- Main entity
		model.addAttribute(MAIN_ENTITY_NAME, car);
		if ( formMode == FormMode.CREATE ) {
			model.addAttribute(MODE, MODE_CREATE); // The form is in "create" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_CREATE); 			
			//--- Other data useful in this screen in "create" mode (all fields)
			populateListOfCarmodelItems(model);
			populateListOfFuelItems(model);
		}
		else if ( formMode == FormMode.UPDATE ) {
			model.addAttribute(MODE, MODE_UPDATE); // The form is in "update" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_UPDATE); 			
			//--- Other data useful in this screen in "update" mode (only non-pk fields)
			populateListOfFuelItems(model);
			populateListOfCarmodelItems(model);
		}
	}
	
	/**
	 * Return last UserRoles
	 * @param Collection<UserRoles> c
	 * @param UserRoles lastElement
	 */
	private UserRoles getUserRolesLastElement(final Collection<UserRoles> c) {
	    final Iterator<UserRoles> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (UserRoles) lastElement;
	}

	//--------------------------------------------------------------------------------------
	// Request mapping
	//--------------------------------------------------------------------------------------
	/**
	 * Shows a list with all the occurrences of Car found in the database
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping()
	public String list(Model model) {
		log("Action 'list'");
		List<Car> list = carService.findAll();
		model.addAttribute(MAIN_LIST_NAME, list);		
		return JSP_LIST;
	}

	public Car getLastElement(final Collection<Car> c) {
	    final Iterator<Car> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (Car) lastElement;
	}
	
	/**
	 * Shows a form page in order to create a new Car
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping("/form")
	public String formForCreate(Model model, @RequestParam(value = "fromDriver", required=false) Boolean fromDriver) {
		log("Action 'formForCreate'");
		//--- Populates the model with a new instance
		Car car = new Car();	
		Integer idcar = 0 ;
		
		List<Car> allCars  = carService.findAll();
		
		if(allCars != null && !allCars.isEmpty()){
			Car lastCar		= getLastElement(allCars);
			idcar 			= lastCar.getId() +1 ;
			car.setId(idcar);
		}
		else{
			car.setId(idcar);
		}
		
		
		List<Brand> listofbrand = brandService.findAll();

		populateModel( model, car, FormMode.CREATE);

		model.addAttribute("listOfBrandItems", listofbrand ) ;;
		
		if(fromDriver != null && fromDriver == true){
			model.addAttribute("fromDriver", fromDriver ) ;
		}
		else{
			model.addAttribute("fromDriver", false ) ;
		}
		return JSP_FORM;
	}

	/**
	 * Shows a form page in order to update an existing Car
	 * @param model Spring MVC model
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/form/{id}")
	public String formForUpdate(Model model, @PathVariable("id") Integer id ) {
		log("Action 'formForUpdate'");
		//--- Search the entity by its primary key and stores it in the model 
		Car car = carService.findById(id);
		
		Integer carmodelId = car.getCarmodelId() ;
		Carmodel carmodel = carmodelService.findById(carmodelId) ;
		
		BrandEntity brand = carmodel.getBrand() ;
		
		List<Brand> listofbrand = brandService.findAll();
		 
	    List<Carmodel> listOfCarmodel = carmodelService.findByBrand(brand);
		
		populateModel( model, car, FormMode.UPDATE);	
		model.addAttribute("carmodel", carmodel ) ;
		model.addAttribute("listOfCarmodel", listOfCarmodel ) ;
		model.addAttribute("brand", brand ) ;
		model.addAttribute("listOfBrandItems", listofbrand ) ;
		return JSP_FORM;
	}

	/**
	 * 'CREATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param car  entity to be created
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/create" ) // GET or POST
	public String create(@Valid Car car, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, @RequestParam("fromDriver") Boolean fromDriver) throws ParseException {
		log("Action 'create'");

		List<Car> allCars  = carService.findAll();
		Integer idcar = 0 ;
		
		if(allCars != null && !allCars.isEmpty()){
			Car lastCar		= getLastElement(allCars) ;
			idcar 			= lastCar.getId() +1 ;
			car.setId(idcar);
		}
		else{
			car.setId(idcar);
		}
		
		List<Brand> listofbrand = brandService.findAll();
		
		try {
			if (!bindingResult.hasErrors()) {
				Car carCreated = carService.create(car); 
				model.addAttribute(MAIN_ENTITY_NAME, carCreated);
				
				// On recupere le user connecté
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = userService.findByUsername(auth.getName());
				
				// On ajoute la nouvelle voiture au user
				user.setCarId(car.getId());			
				// Et on sauvegarde le user en base
				userService.update(user);
				
				// On regarde si on vient du formulaire de création d'un conducteur on si c'est une création d'un user deja conducteur
				if(fromDriver){			
					// On donne le role ROLE_DRIVER au user
					UserRoles userRoles = new UserRoles() ;
					List<UserRoles> allUsersRoles  = userRolesService.findAll();
					if(allUsersRoles != null && !allUsersRoles.isEmpty()){
						UserRoles lastUserRoles		 = getUserRolesLastElement(allUsersRoles);
						userRoles.setId(lastUserRoles.getId()+1);	
					}
					else{
						userRoles.setId(0);
					}
					
					// On verifie si le user n'est pas deja conducteur
					List<UserRoles> listUserRoles = userRolesService.findByUsername(user.getUsername());
					Boolean isDriver 			  = false ;
					for ( UserRoles role : listUserRoles ) {
						if(role.getRole().equalsIgnoreCase("ROLE_DRIVER")){
							isDriver = true ;
						}
					}
					
					// Si le user n'est pas encore conducteur on le met conducteur
					if(!isDriver){
						userRoles.setRole("ROLE_DRIVER");
						userRoles.setUsername(user.getUsername());
						userRolesService.create(userRoles); 			
					}
					
					messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"index.becomeDriverSuccess"));
					return "redirect:/";
				}
				else{
					return redirectToForm(httpServletRequest, car.getId() );
				}
			} else {
				
				populateModel( model, car, FormMode.CREATE);	
				model.addAttribute("listOfBrandItems", listofbrand ) ;				
				return JSP_FORM;
			}
		} catch(Exception e) {
			log("Action 'create' : Exception - " + e.getMessage() );
			messageHelper.addException(model, "car.error.create", e);
			populateModel( model, car, FormMode.CREATE);
			model.addAttribute("listOfBrandItems", listofbrand ) ;	
			return JSP_FORM;
		}
	}

	/**
	 * 'UPDATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param car  entity to be updated
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/update" ) // GET or POST
	public String update(@Valid Car car, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'update'");
		
		List<Brand> listofbrand = brandService.findAll();
	    
		try {
			if (!bindingResult.hasErrors()) {
				//--- Perform database operations
				Car carSaved = carService.update(car);
				model.addAttribute(MAIN_ENTITY_NAME, carSaved);
				//--- Set the result message
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				log("Action 'update' : update done - redirect");
				return redirectToForm(httpServletRequest, car.getId());
			} else {
				log("Action 'update' : binding errors");
			    
				populateModel( model, car, FormMode.UPDATE);
				model.addAttribute("listOfBrandItems", listofbrand ) ;
				
				return JSP_FORM;
			}
		} catch(Exception e) {
			messageHelper.addException(model, "car.error.update", e);
			log("Action 'update' : Exception - " + e.getMessage() );
			populateModel( model, car, FormMode.UPDATE);
			model.addAttribute("listOfBrandItems", listofbrand ) ;
			return JSP_FORM;
		}
	}

	/**
	 * 'DELETE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param redirectAttributes
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}") // GET or POST
	public String delete(RedirectAttributes redirectAttributes, @PathVariable("id") Integer id) {
		log("Action 'delete'" );
		try {
			carService.delete( id );
			//--- Set the result message
			messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));	
		} catch(Exception e) {
			messageHelper.addException(redirectAttributes, "car.error.delete", e);
		}
		return redirectToList();
	}
	
	
	@RequestMapping(produces = MediaType.TEXT_HTML_VALUE, value = "/majCarModel/{brandid}") // GET or POST
	public @ResponseBody String majCarModel(RedirectAttributes redirectAttributes, @PathVariable("brandid") Integer brandid, HttpServletResponse response) {

	    response.setContentType("text/html");
	    response.setCharacterEncoding("UTF-8");
	    
	    BrandEntity brand = brandService.findEntityById(brandid) ;
	    
	    List<Carmodel> list = carmodelService.findByBrand(brand);
	    
    
	    String html = "<select name='carmodelId' class='form-control'>" ;
	    html 		+= 		"<option></option>" ;
	    
	    for (final Carmodel carmodel : list) {
	    	html 	+= 		"<option value='"+carmodel.getId()+"'>"+carmodel.getName()+"</option>" ;
	    }
	    
	    html 		+= "</select>" ;
	    html 		+= "<br>" ;
	    
	    
		return html ;
	}

}
