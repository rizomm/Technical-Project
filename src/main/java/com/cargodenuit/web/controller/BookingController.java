
package com.cargodenuit.web.controller;

import com.cargodenuit.business.service.*;
import com.cargodenuit.entity.*;
import com.cargodenuit.entity.jpa.BrandEntity;
import com.cargodenuit.web.common.AbstractController;
import com.cargodenuit.web.common.FormMode;
import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageType;
import com.cargodenuit.web.listitem.TripListItem;
import com.cargodenuit.web.listitem.UserListItem;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

//--- Common classes
//--- Entities
//--- Services
//--- List Items

/**
 * Spring MVC controller for 'Booking' management.
 */
@Controller
@RequestMapping("/booking")
public class BookingController extends AbstractController {

	//--- Variables names ( to be used in JSP with Expression Language )
	private static final String MAIN_ENTITY_NAME = "booking";
	private static final String MAIN_LIST_NAME   = "list";

	//--- JSP pages names ( View name in the MVC model )
	private static final String JSP_FORM   		= "booking/form";
	private static final String JSP_LIST   		= "booking/list";
	private static final String JSP_VIEW_TRIP   = "trip/view";

	//--- SAVE ACTION ( in the HTML form )
	private static final String SAVE_ACTION_CREATE   = "/booking/create";
	private static final String SAVE_ACTION_UPDATE   = "/booking/update";

	//--- Main entity service
	@Resource
    private BookingService bookingService;
	//--- Other service(s)
	@Resource
    private UserService userService;
	@Resource
    private TripService tripService;
	@Resource
    private CarService carService;
	@Resource
    private CarmodelService carmodelService;
	@Resource
    private BrandService brandService;
	@Resource
    private FuelService fuelService;

	//--------------------------------------------------------------------------------------
	/**
	 * Default constructor
	 */
	public BookingController() {
		super(BookingController.class, MAIN_ENTITY_NAME );
		log("BookingController created.");
	}

	//--------------------------------------------------------------------------------------
	// Spring MVC model management
	//--------------------------------------------------------------------------------------
	/**
	 * Populates the combo-box "items" for the referenced entity "User"
	 * @param model
	 */
	private void populateListOfUserItems(Model model) {
		List<User> list = userService.findAll();
		List<UserListItem> items = new LinkedList<UserListItem>();
		for ( User user : list ) {
			items.add(new UserListItem( user ) );
		}
		model.addAttribute("listOfUserItems", items ) ;
	}

	/**
	 * Populates the combo-box "items" for the referenced entity "Trip"
	 * @param model
	 */
	private void populateListOfTripItems(Model model) {
		List<Trip> list = tripService.findAll();
		List<TripListItem> items = new LinkedList<TripListItem>();
		for ( Trip trip : list ) {
			items.add(new TripListItem( trip ) );
		}
		model.addAttribute("listOfTripItems", items ) ;
	}


	/**
	 * Populates the Spring MVC model with the given entity and eventually other useful data
	 * @param model
	 * @param booking
	 */
	private void populateModel(Model model, Booking booking, FormMode formMode) {
		//--- Main entity
		model.addAttribute(MAIN_ENTITY_NAME, booking);
		if ( formMode == FormMode.CREATE ) {
			model.addAttribute(MODE, MODE_CREATE); // The form is in "create" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_CREATE); 			
			//--- Other data useful in this screen in "create" mode (all fields)
			populateListOfUserItems(model);
			populateListOfTripItems(model);
		}
		else if ( formMode == FormMode.UPDATE ) {
			model.addAttribute(MODE, MODE_UPDATE); // The form is in "update" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_UPDATE); 			
			//--- Other data useful in this screen in "update" mode (only non-pk fields)
			populateListOfTripItems(model);
			populateListOfUserItems(model);
		}
	}

	//--------------------------------------------------------------------------------------
	// Request mapping
	//--------------------------------------------------------------------------------------
	/**
	 * Shows a list with all the occurrences of Booking found in the database
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping("/list")
	public String list(Model model, HttpServletRequest request) {
		log("Action 'list'");
		List<Booking> list = bookingService.findAll();
		model.addAttribute(MAIN_LIST_NAME, list);	
		
		String message2 = request.getParameter("message");
		model.addAttribute("message2", message2);	
		
		return JSP_LIST;
	}
	
	public Booking getLastElement(final Collection<Booking> c) {
	    final Iterator<Booking> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (Booking) lastElement;
	}
	
	/**
	 * Shows a form page in order to create a new Booking
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping("/form")
	public String formForCreate(Model model) {
		log("Action 'formForCreate'");
		//--- Populates the model with a new instance
		Booking booking = new Booking();	
		
		List<Booking> allBookings  = bookingService.findAll();
		
		/**
		 *  On attribut un id à la nouvelle entitée
		 *  Si il n'existe pas encore d'entité, c'est donc la première et on lui affecte l'id 0
		 */
		if(allBookings != null && !allBookings.isEmpty()){
			Booking lastBooking		 = getLastElement(allBookings);
			booking.setId(lastBooking.getId()+1);
		}
		else{
			booking.setId(0);
		}
		
		populateModel( model, booking, FormMode.CREATE);
		return JSP_FORM;
	}

	/**
	 * Shows a form page in order to update an existing Booking
	 * @param model Spring MVC model
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/form/{id}")
	public String formForUpdate(Model model, @PathVariable("id") Integer id ) {
		log("Action 'formForUpdate'");
		//--- Search the entity by its primary key and stores it in the model 
		Booking booking = bookingService.findById(id);
		populateModel( model, booking, FormMode.UPDATE);		
		return JSP_FORM;
	}

	/**
	 * 'CREATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param booking  entity to be created
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/create" ) // GET or POST
	public String create(@Valid Booking booking, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'create'");
		try {
			if (!bindingResult.hasErrors()) {
				Booking bookingCreated = bookingService.create(booking); 
				model.addAttribute(MAIN_ENTITY_NAME, bookingCreated);

				//---
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				return redirectToForm(httpServletRequest, booking.getId() );
			} else {
				populateModel( model, booking, FormMode.CREATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			log("Action 'create' : Exception - " + e.getMessage() );
			messageHelper.addException(model, "booking.error.create", e);
			populateModel( model, booking, FormMode.CREATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'UPDATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param booking  entity to be updated
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/update" ) // GET or POST
	public String update(@Valid Booking booking, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'update'");
		try {
			if (!bindingResult.hasErrors()) {
				//--- Perform database operations
				Booking bookingSaved = bookingService.update(booking);
				model.addAttribute(MAIN_ENTITY_NAME, bookingSaved);
				//--- Set the result message
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				log("Action 'update' : update done - redirect");
				return redirectToForm(httpServletRequest, booking.getId());
			} else {
				log("Action 'update' : binding errors");
				populateModel( model, booking, FormMode.UPDATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			messageHelper.addException(model, "booking.error.update", e);
			log("Action 'update' : Exception - " + e.getMessage() );
			populateModel( model, booking, FormMode.UPDATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'DELETE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param redirectAttributes
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}") // GET or POST
	public String delete(RedirectAttributes redirectAttributes, @PathVariable("id") Integer id) {
		log("Action 'delete'" );
		try {
			// On va remettre les sièges dans le Trip
			Booking booking = bookingService.findById(id);
			Trip trip 		= tripService.findByBookingId(id);
			trip.setNumberofseat(trip.getNumberofseat() + booking.getNumberofseat());
			tripService.update(trip);
			
			// On supprime enfin le Booking
			bookingService.delete( id );
			//--- Set the result message
			messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));	
		} catch(Exception e) {
			messageHelper.addException(redirectAttributes, "booking.error.delete", e);
		}
		return redirectToList();
	}
	
	/**
	 * Fonction pour calculer l'age d'une voiture
	 *
	 * @param Date d La date de mise en service de la voiture 
	 * @return Integer yeardiff L'age de la voiture
	 */
	public static int calculAge(Date d)
	{
	  Calendar curr = Calendar.getInstance();
	  Calendar birth = Calendar.getInstance();
	  birth.setTime(d);
	  int yeardiff = curr.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
	  curr.add(Calendar.YEAR,-yeardiff);
	  if(birth.after(curr))
	  {
	    yeardiff = yeardiff - 1;
	  }
	  return yeardiff;
	}
	
	@RequestMapping("/searchtrip")
	public ModelAndView formForSearchTrip() {
		log("Action 'formForSearchTrip'");
		
		ModelAndView formForSearchTrip =  new ModelAndView("searchtrip") ;
		
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 	= userService.findByUsername(auth.getName());
		
		// On recupere tous les Trips
		List<Trip> allTrips  = tripService.findAll();
		
		// On va garder que les Trips qui ont une date supperieur à aujourd'hui
		List<Trip> allTripsWithDateSuppToday  = new ArrayList<Trip>();
		Date today = new Date() ;
		for(Trip trip : allTrips){
			if(trip.getStartdate().after(today)){
				if(trip.getUserId() != user.getId()){ // On ne garde que les trips qui ne sont pas liés au user
					if(trip.getNumberofseat() > 0){ // On ne garde que les trips qui ont des places libres
						allTripsWithDateSuppToday.add(trip); 						
					}
				}
			}
		}
		
		List<User> allUsers  = userService.findAll();

		formForSearchTrip.addObject("listTrips", allTripsWithDateSuppToday);
		formForSearchTrip.addObject("allUsers", allUsers);
		
		return formForSearchTrip;
	}
	
	@RequestMapping("/trip/{idTrip}")
	public ModelAndView bookATrip( RedirectAttributes redirectAttributes, HttpServletRequest request,
			@PathVariable( "idTrip" ) int idTrip ) {
		log("Action 'formForSearchTrip'");
		ModelAndView viewTrip =  new ModelAndView(JSP_VIEW_TRIP) ;
				
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 			= userService.findByUsername(auth.getName());
		
		// On recupere le Trip
		//String tripId 		= request.getParameter("tripId");
		Trip trip  			= tripService.findById(idTrip);
		

		// On recupere le user lié au Trip
		Integer tripUserId 	= trip.getUserId();
		User tripUser 		= userService.findById(tripUserId);

		// On recupere le Car du tripUser
		Integer carId 		= tripUser.getCarId();
		Car car 			= carService.findById(carId);
		
		// On recupere le carburant
		Integer fuelId 		= car.getFuelId();
		Fuel fuel 			= fuelService.findById(fuelId);
		
		// On recupere le Carmodel
		Integer carModelId 	= car.getCarmodelId();
		Carmodel carmodel	= carmodelService.findById(carModelId);
	
		// On recupere la Brand
		BrandEntity brand = carmodel.getBrand();
		
		// On calcul l'age de la voiture
		Date issuancedate 	= car.getIssuancedate();
		int carAge 			= calculAge(issuancedate);
		
		// On recupere les parametres ajax
		String enddate 		= request.getParameter("enddate");
		String duration 	= request.getParameter("duration");
		String distance 	= request.getParameter("distance");
		String addressStart = request.getParameter("addressStart");
		String addressEnd 	= request.getParameter("addressEnd");
		String numberOfSeat = request.getParameter("numberOfSeat");
		
		// Si les parametres sont nul, c'est que le user a pris directement la reservation,
		// Donc les parametres sont ceux du Trip
		if(enddate == ""){
			enddate = trip.getEnddate().toString();
		}
		if(duration == ""){
			long duartionTime = trip.getEnddate().getTime() - trip.getStartdate().getTime();
			long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duartionTime);
			long diffInHours = TimeUnit.MILLISECONDS.toHours(duartionTime);
			duration = "" ;
			if(diffInHours > 0){
				duration += diffInHours + "h " ;
			}
			if(diffInMinutes > 0){
				duration += diffInMinutes + "min " ;
			}
		}
		if(addressStart == ""){
			addressStart = trip.getAddressStart() ;
		}
		if(addressEnd == ""){
			addressEnd = trip.getAddressEnd() ;
		}
		if(distance == ""){
			distance = trip.getDistance().toString() ;
		}
		else{
			distance = distance.substring(0, distance.length()-4) ;
		}
		if(numberOfSeat == ""){
			numberOfSeat = trip.getNumberofseat().toString() ;
		}
		
		// On formate les dates
		String startdate = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(trip.getStartdate());

		
		/**
		 * Pour calculer le prix, on prend le prix total et on fait le prorata de la distance
		 * multiplié par le nombre de sieges
		 */
		float prorata = (float)Integer.parseInt(distance) / (float)trip.getDistance() ;
		float cost  = trip.getCost().floatValue() * prorata * Integer.parseInt(numberOfSeat) ;
		// On ne garde que 2 chiffres apres la virgule
		BigDecimal cost2 = BigDecimal.valueOf(cost);
		BigDecimal cost3 = cost2.setScale(2, BigDecimal.ROUND_FLOOR);
		
		viewTrip.addObject("user", user);
		viewTrip.addObject("trip", trip);
		viewTrip.addObject("tripUser", tripUser);
		viewTrip.addObject("car", car);
		viewTrip.addObject("carmodel", carmodel);		
		viewTrip.addObject("fuel", fuel);
		viewTrip.addObject("brand", brand);
		viewTrip.addObject("carAge", carAge);
		viewTrip.addObject("numberOfSeat", numberOfSeat);
		viewTrip.addObject("cost", cost3);
		
		viewTrip.addObject("enddate", enddate);
		viewTrip.addObject("duration", duration);
		viewTrip.addObject("distance", distance);
		viewTrip.addObject("addressStart", addressStart);
		viewTrip.addObject("addressEnd", addressEnd);
		viewTrip.addObject("startdate", startdate);
		
		return viewTrip;
	}
	
	@RequestMapping("/add")
	public String confirmTrip(HttpServletRequest request) throws ParseException {
		log("Action 'Confirm Trip'");
		
		
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 			= userService.findByUsername(auth.getName());
		
		// On recupere les parametres ajax
		String cost 		= request.getParameter("cost");
		String distance 	= request.getParameter("distance");
		String addressStart = request.getParameter("addressStart");
		String addressEnd 	= request.getParameter("addressEnd");
		String numberOfSeat = request.getParameter("numberOfSeat");
		String enddate 		= request.getParameter("enddate");
		String startdate	= request.getParameter("startdate");
		String tripId 		= request.getParameter("tripId");
		
		Integer userId 		= user.getId();
		

		// On créé la nouvelle entité
		Booking booking = new Booking();	
				
		/**
		 *  On attribut un id à la nouvelle entitée
		 *  Si il n'existe pas encore d'entité, c'est donc la première et on lui affecte l'id 0
		 */
		List<Booking> allBookings  = bookingService.findAll();
		if(allBookings != null && !allBookings.isEmpty()){
			Booking lastBooking		 = getLastElement(allBookings);
			booking.setId(lastBooking.getId()+1);
		}
		else{
			booking.setId(0);
		}
		

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		
		booking.setAddressEnd(addressEnd);
		booking.setAddressStart(addressStart);
		booking.setDistance(Integer.parseInt(distance));
		booking.setEnddate(formatter.parse(enddate));
		booking.setNumberofseat(Integer.parseInt(numberOfSeat));
		booking.setStartdate(formatter.parse(startdate));
		booking.setTripId(Integer.parseInt(tripId));
		booking.setUserId(userId);
		booking.setEvaluated(false);
		//On parse le string en Bigdecimal
		//Create a DecimalFormat that fits your requirements
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		String pattern = "##0.0#";

		
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		// parse the string
		BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(cost);
		booking.setCost(bigDecimal);

		
		bookingService.create(booking); 
		
		// On déduit les places reservées
		Trip trip = tripService.findById(Integer.parseInt(tripId));
		trip.setNumberofseat(trip.getNumberofseat() - Integer.parseInt(numberOfSeat));
		tripService.update(trip);
		
		
		return "redirect:/userHistory/history";
	}

}
