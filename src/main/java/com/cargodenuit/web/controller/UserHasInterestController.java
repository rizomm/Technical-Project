/*
 * Created on 27 f�vr. 2015 ( Time 17:36:14 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.cargodenuit.web.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//--- Common classes
import com.cargodenuit.web.common.AbstractController;
import com.cargodenuit.web.common.FormMode;
import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageType;

//--- Entities
import com.cargodenuit.entity.UserHasInterest;

//--- Services 
import com.cargodenuit.business.service.UserHasInterestService;


/**
 * Spring MVC controller for 'UserHasInterest' management.
 */
@Controller
@RequestMapping("/userHasInterest")
public class UserHasInterestController extends AbstractController {

	//--- Variables names ( to be used in JSP with Expression Language )
	private static final String MAIN_ENTITY_NAME = "userHasInterest";
	private static final String MAIN_LIST_NAME   = "list";

	//--- JSP pages names ( View name in the MVC model )
	private static final String JSP_FORM   = "userHasInterest/form";
	private static final String JSP_LIST   = "userHasInterest/list";

	//--- SAVE ACTION ( in the HTML form )
	private static final String SAVE_ACTION_CREATE   = "/userHasInterest/create";
	private static final String SAVE_ACTION_UPDATE   = "/userHasInterest/update";

	//--- Main entity service
	@Resource
    private UserHasInterestService userHasInterestService; // Injected by Spring
	//--- Other service(s)

	//--------------------------------------------------------------------------------------
	/**
	 * Default constructor
	 */
	public UserHasInterestController() {
		super(UserHasInterestController.class, MAIN_ENTITY_NAME );
		log("UserHasInterestController created.");
	}

	//--------------------------------------------------------------------------------------
	// Spring MVC model management
	//--------------------------------------------------------------------------------------

	/**
	 * Populates the Spring MVC model with the given entity and eventually other useful data
	 * @param model
	 * @param userHasInterest
	 */
	private void populateModel(Model model, UserHasInterest userHasInterest, FormMode formMode) {
		//--- Main entity
		model.addAttribute(MAIN_ENTITY_NAME, userHasInterest);
		if ( formMode == FormMode.CREATE ) {
			model.addAttribute(MODE, MODE_CREATE); // The form is in "create" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_CREATE); 			
			//--- Other data useful in this screen in "create" mode (all fields)
		}
		else if ( formMode == FormMode.UPDATE ) {
			model.addAttribute(MODE, MODE_UPDATE); // The form is in "update" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_UPDATE); 			
			//--- Other data useful in this screen in "update" mode (only non-pk fields)
		}
	}

	//--------------------------------------------------------------------------------------
	// Request mapping
	//--------------------------------------------------------------------------------------
	/**
	 * Shows a list with all the occurrences of UserHasInterest found in the database
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping()
	public String list(Model model) {
		log("Action 'list'");
		List<UserHasInterest> list = userHasInterestService.findAll();
		model.addAttribute(MAIN_LIST_NAME, list);		
		return JSP_LIST;
	}

	/**
	 * Shows a form page in order to create a new UserHasInterest
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping("/form")
	public String formForCreate(Model model) {
		log("Action 'formForCreate'");
		//--- Populates the model with a new instance
		UserHasInterest userHasInterest = new UserHasInterest();	
		populateModel( model, userHasInterest, FormMode.CREATE);
		return JSP_FORM;
	}

	/**
	 * Shows a form page in order to update an existing UserHasInterest
	 * @param model Spring MVC model
	 * @param userId  primary key element
	 * @param interestId  primary key element
	 * @return
	 */
	@RequestMapping(value = "/form/{userId}/{interestId}")
	public String formForUpdate(Model model, @PathVariable("userId") Integer userId, @PathVariable("interestId") Integer interestId ) {
		log("Action 'formForUpdate'");
		//--- Search the entity by its primary key and stores it in the model 
		UserHasInterest userHasInterest = userHasInterestService.findById(userId, interestId);
		populateModel( model, userHasInterest, FormMode.UPDATE);		
		return JSP_FORM;
	}

	/**
	 * 'CREATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param userHasInterest  entity to be created
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/create" ) // GET or POST
	public String create(@Valid UserHasInterest userHasInterest, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'create'");
		try {
			if (!bindingResult.hasErrors()) {
				UserHasInterest userHasInterestCreated = userHasInterestService.create(userHasInterest); 
				model.addAttribute(MAIN_ENTITY_NAME, userHasInterestCreated);

				//---
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				return redirectToForm(httpServletRequest, userHasInterest.getUserId(), userHasInterest.getInterestId() );
			} else {
				populateModel( model, userHasInterest, FormMode.CREATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			log("Action 'create' : Exception - " + e.getMessage() );
			messageHelper.addException(model, "userHasInterest.error.create", e);
			populateModel( model, userHasInterest, FormMode.CREATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'UPDATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param userHasInterest  entity to be updated
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/update" ) // GET or POST
	public String update(@Valid UserHasInterest userHasInterest, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'update'");
		try {
			if (!bindingResult.hasErrors()) {
				//--- Perform database operations
				UserHasInterest userHasInterestSaved = userHasInterestService.update(userHasInterest);
				model.addAttribute(MAIN_ENTITY_NAME, userHasInterestSaved);
				//--- Set the result message
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				log("Action 'update' : update done - redirect");
				return redirectToForm(httpServletRequest, userHasInterest.getUserId(), userHasInterest.getInterestId());
			} else {
				log("Action 'update' : binding errors");
				populateModel( model, userHasInterest, FormMode.UPDATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			messageHelper.addException(model, "userHasInterest.error.update", e);
			log("Action 'update' : Exception - " + e.getMessage() );
			populateModel( model, userHasInterest, FormMode.UPDATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'DELETE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param redirectAttributes
	 * @param userId  primary key element
	 * @param interestId  primary key element
	 * @return
	 */
	@RequestMapping(value = "/delete/{userId}/{interestId}") // GET or POST
	public String delete(RedirectAttributes redirectAttributes, @PathVariable("userId") Integer userId, @PathVariable("interestId") Integer interestId) {
		log("Action 'delete'" );
		try {
			userHasInterestService.delete( userId, interestId );
			//--- Set the result message
			messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));	
		} catch(Exception e) {
			messageHelper.addException(redirectAttributes, "userHasInterest.error.delete", e);
		}
		return redirectToList();
	}

}
