package com.cargodenuit.web.controller;

import com.cargodenuit.business.service.BookingService;
import com.cargodenuit.business.service.TripService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ecockenp on 27/03/2015 for Technical-Project.
 */
@Controller
@RequestMapping( "/userHistory" )
public class HistoryController {

    @Resource
    private UserService userService;

    @Resource
    private BookingService bookingService;

    @Resource
    private TripService tripService;

    private static final String HISTORY_PAGE = "history/page";

    @RequestMapping( "/history" )
    public ModelAndView UserHistoryBooking() {

        ModelAndView BookingPage = new ModelAndView( HISTORY_PAGE );
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.findByUsername( auth.getName() );

        HashMap<Booking, User> bookingUserMap = new HashMap<Booking, User>();

        List<Booking> pastBookingList = bookingService.pastBookingByUserId( user.getId() );
        List<Booking> futureBookingList = bookingService.futureBookingByUserId( user.getId() );

        for ( Booking booking : pastBookingList ) {
            User driver = tripService.getDriver( tripService.findById( booking.getTripId() ) );
            bookingUserMap.put( booking ,driver );
        }

        BookingPage.addObject( "bookingUserMap",bookingUserMap );
        BookingPage.addObject( "futureBookingList", futureBookingList );

        return BookingPage;
    }
    //
    //    @RequestMapping( "/inProgress" )
    //    public ModelAndView UserInProgressTrip() {
    //
    //        ModelAndView futureTripPage = new ModelAndView( HISTORY_PAGE );
    //        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    //        User user = userService.findByUsername( auth.getName() );
    //        futureTripPage.addObject( "user", user );
    //
    //        List<Booking> futureBookingList = bookingService.futureBookingByUserId( user.getId() );
    //
    //        futureTripPage.addObject( "futureBookingList", futureBookingList );
    //        return futureTripPage;
    //    }
}
