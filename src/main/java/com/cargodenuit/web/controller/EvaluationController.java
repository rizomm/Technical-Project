package com.cargodenuit.web.controller;

import com.cargodenuit.business.service.BookingService;
import com.cargodenuit.business.service.MarkService;
import com.cargodenuit.business.service.TripService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.Mark;
import com.cargodenuit.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by ecockenp on 31/03/2015 for Technical-Project.
 * Evaluation Controller .
 */

@Controller
@RequestMapping( "/mark" )
public class EvaluationController {

    private static final String FORM_MARK_PAGE = "evaluation/form";
    private static final String HISTORY_PAGE   = "history/page";

    @Resource
    private BookingService bookingService;

    @Resource
    private TripService tripService;

    @Resource
    private MarkService markService;

    @Resource
    private UserService userService;

    @RequestMapping( "requestEvaluationDriver/{id}" )
    public ModelAndView request4Mark( @PathVariable( "id" ) int BookingId ) {
        Mark mark = new Mark();
        Booking booking = bookingService.findById( BookingId );
        User driver = tripService.getDriver( tripService.findById( booking.getTripId() ) );
        ModelAndView request4Markpage = new ModelAndView( FORM_MARK_PAGE );
        System.out.println( "requestEvaluationDriver ,booking ID " + BookingId );
        request4Markpage.addObject( "booking", booking );
        request4Markpage.addObject( "driver", driver );
        request4Markpage.addObject( "mark", mark );

        return request4Markpage;
    }

    /**
     * Controller that permit to the passenger to submit the evaluation and update the driver's mark , according to his trip.
     *
     * @param mark          submit by the form it is split in 3 .( Amability / Punctuality / Skill )
     * @param bindingResult check if the mark obkect submitted by the form fit with the model.
     * @param bookingId     the booking id is the id that permit to identify , the booking , the trip , the driver .
     * @return ModelAndView return the listing page with the followings objects inside : booking ,driver ,pastBookingList ,futureBookingList
     */
    @RequestMapping( "requestEvaluationDriver/submitEvaluationForm/{id}" )
    public String submitEvaluation( @Valid Mark mark, BindingResult bindingResult,
            @PathVariable( "id" ) int bookingId ) {

        //        System.out.println( "####### /submitEvaluationForm/" + bookingId + " ####### " );

        if ( bindingResult.hasErrors() ) {
            System.out.println( "####### DEBUG : Model Has Errors ####### " );
            return "redirect:/requestEvaluationDriver/"+bookingId;
        } else {
            System.out.println( "####### DEBUG : Model Has No Errors ####### " );
            ModelAndView formMark = new ModelAndView( HISTORY_PAGE );

            Booking booking = bookingService.findById( bookingId );
            User driver = tripService.getDriver( tripService.findById( booking.getTripId() ) );
            float averageMarkDriverByBooking = markService
                    .averageMarkDriverByBooking( mark.getAmabilityMark(), mark.getDriverSkillMark(),
                            mark.getPonctualityMark() );

            averageMarkDriverByBooking = Math.round( averageMarkDriverByBooking - 0.5f );

            if ( driver.getMark() == null ) {
                driver.setMark( BigDecimal.valueOf( ( averageMarkDriverByBooking ) ) );
            } else {
                float currentDriverMark = driver.getMark().floatValue();

                float driverMark = markService.averageMarkByDriver( averageMarkDriverByBooking, currentDriverMark );

                BigDecimal driverMark2 = BigDecimal.valueOf( driverMark );
                BigDecimal driverMark3 = driverMark2.setScale( 1, BigDecimal.ROUND_FLOOR );

                driver.setMark( driverMark3 );
            }

            booking.setEvaluated( true );
            bookingService.update( booking );
            userService.update( driver );

            formMark.addObject( "booking", booking );
            formMark.addObject( "driver", driver );

            List<Booking> pastBookingList = bookingService.pastBookingByUserId( booking.getUserId() );
            List<Booking> futureBookingList = bookingService.futureBookingByUserId( booking.getUserId() );

            formMark.addObject( "pastBookingList", pastBookingList );
            formMark.addObject( "futureBookingList", futureBookingList );
            return "redirect:/userHistory/history";
        }

    }
    //
    //    @RequestMapping( " requestEvaluationDriver/{TripId}/{mark}/{UserId}" )
    //    public ModelAndView submitAfterMark( @PathVariable( "mark" ) float mark, @PathVariable( "UserId" ) int UserId,
    //            @PathVariable( "TripId" ) int TripId ) {
    //
    //        ModelAndView bookingPage = new ModelAndView( HISTORY_PAGE );
    //        User driverMarked = userService.findById( UserId );
    //        driverMarked.setMark( BigDecimal.valueOf( mark ) );
    //
    //        Booking bookingMarked = bookingService.findById( UserId );
    //        bookingMarked.setEvaluated( true );
    //
    //        return bookingPage;
    //    }

}
