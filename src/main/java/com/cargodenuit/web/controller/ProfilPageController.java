package com.cargodenuit.web.controller;

import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by ecockenp on 07/04/2015 for Technical-Project.
 * ProfilePageController
 */

@Controller
@RequestMapping( "/profil" )
public class ProfilPageController {

    @Resource
    UserService userService;
    private static final String JSP_PROFILPAGE = "profil/page";

    @RequestMapping( "/account" )
    public ModelAndView userProfilPage() {
        ModelAndView profilPage = new ModelAndView( JSP_PROFILPAGE );
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername( auth.getName() );

        profilPage.addObject( "user", user );
        return profilPage;
    }

    @RequestMapping( "/profilePage/{id}" )
    public ModelAndView userProfilPage( @PathVariable( "id" ) int id ) {
        ModelAndView profilPage = new ModelAndView( JSP_PROFILPAGE );
        


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userConnected = userService.findByUsername( auth.getName() );
        
        User userVisited = userService.findById( id );

        float affinity = userService.affinity( userConnected, userVisited );
        
        profilPage.addObject( "affinity", affinity );
        profilPage.addObject( "userVisited", userVisited );

        return profilPage;
    }

}
