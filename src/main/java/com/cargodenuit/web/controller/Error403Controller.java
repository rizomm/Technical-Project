package com.cargodenuit.web.controller;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.User;
 
@Controller
@RequestMapping("/403")
public class Error403Controller {
	
	@Resource
    private UserService userService;
 
	@RequestMapping(value = "")
	public ModelAndView error403() {
 
		ModelAndView model = new ModelAndView("error403");
		
		
		// On recupere le user connecté
		Authentication auth 		= SecurityContextHolder.getContext().getAuthentication();
		User user 					= userService.findByUsername(auth.getName());
	    
		
		
		model.addObject("user", user);

 
		return model;
 
	}
 
}