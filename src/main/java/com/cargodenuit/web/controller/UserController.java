package com.cargodenuit.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cargodenuit.business.service.UserRolesService;
import com.cargodenuit.entity.UserRoles;
//--- Common classes
import com.cargodenuit.web.common.AbstractController;
import com.cargodenuit.web.common.FormMode;
import com.cargodenuit.web.common.Message;
import com.cargodenuit.web.common.MessageType;
import com.cargodenuit.entity.Brand;
import com.cargodenuit.entity.Carmodel;
import com.cargodenuit.entity.Interest;
import com.cargodenuit.entity.Music;
//--- Entities
import com.cargodenuit.entity.User;
import com.cargodenuit.entity.Car;
import com.cargodenuit.entity.UserHasInterest;
import com.cargodenuit.entity.UserHasMusic;
import com.cargodenuit.business.service.BrandService;
import com.cargodenuit.business.service.CarmodelService;
import com.cargodenuit.business.service.FuelService;
import com.cargodenuit.business.service.InterestService;
import com.cargodenuit.business.service.MusicService;
import com.cargodenuit.business.service.UserHasInterestService;
import com.cargodenuit.business.service.UserHasMusicService;
//--- Services 
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.business.service.CarService;

//--- List Items 
import com.cargodenuit.web.listitem.CarListItem;

/**
 * Spring MVC controller for 'User' management.
 */
@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	//--- Variables names ( to be used in JSP with Expression Language )
	private static final String MAIN_ENTITY_NAME = "user";
	private static final String MAIN_LIST_NAME   = "list";

	//--- JSP pages names ( View name in the MVC model )
	private static final String JSP_FORM   			= "user/form";
	private static final String JSP_DRIVER_FORM   	= "user/driver/form";
	private static final String JSP_PREFERENCE_FORM = "user/preference/form";
	private static final String JSP_LIST   			= "user/list";

	//--- SAVE ACTION ( in the HTML form )
	private static final String SAVE_ACTION_CREATE   = "/user/create";
	private static final String SAVE_ACTION_UPDATE   = "/user/update";
	
	
    RequestCache requestCache;

    @Autowired
    protected AuthenticationManager authenticationManager;

	//--- Main entity service
	@Resource
    private UserService userService; // Injected by Spring
	//--- Other service(s)
	@Resource
    private CarService carService; // Injected by Spring
	
	@Resource
    private UserRolesService userRolesService;
	
	@Resource
    private CarmodelService carmodelService; 
	@Resource
    private FuelService fuelService;
	@Resource
    private BrandService brandService; 
	@Resource
    private InterestService interestService; 
	@Resource
    private MusicService musicService; 
	@Resource
    private UserHasMusicService userHasMusicService ;
	@Resource
    private UserHasInterestService userHasInterestService ;

	//--------------------------------------------------------------------------------------
	/**
	 * Default constructor
	 */
	public UserController() {
		super(UserController.class, MAIN_ENTITY_NAME );
		log("UserController created.");
	}

	//--------------------------------------------------------------------------------------
	// Spring MVC model management
	//--------------------------------------------------------------------------------------

	/**
	 * Populates the combo-box "items" for the referenced entity "Car"
	 * @param model
	 */
	private void populateListOfCarItems(Model model) {
		List<Car> list = carService.findAll();
		List<CarListItem> items = new LinkedList<CarListItem>();
		for ( Car car : list ) {
			items.add(new CarListItem( car ) );
		}
		model.addAttribute("listOfCarItems", items ) ;
	}


	/**
	 * Populates the Spring MVC model with the given entity and eventually other useful data
	 * @param model
	 * @param user
	 */
	private void populateModel(Model model, User user, FormMode formMode) {
		//--- Main entity
		model.addAttribute(MAIN_ENTITY_NAME, user);
		if ( formMode == FormMode.CREATE ) {
			model.addAttribute(MODE, MODE_CREATE); // The form is in "create" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_CREATE); 			
			//--- Other data useful in this screen in "create" mode (all fields)
			populateListOfCarItems(model);
		}
		else if ( formMode == FormMode.UPDATE ) {
			model.addAttribute(MODE, MODE_UPDATE); // The form is in "update" mode
			model.addAttribute(SAVE_ACTION, SAVE_ACTION_UPDATE); 			
			//--- Other data useful in this screen in "update" mode (only non-pk fields)
			populateListOfCarItems(model);
		}
	}
	
	
	private UserRoles getUserRolesLastElement(final Collection<UserRoles> c) {
	    final Iterator<UserRoles> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (UserRoles) lastElement;
	}
	
	public Car getLastCarElement(final Collection<Car> c) {
	    final Iterator<Car> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (Car) lastElement;
	}
	
	public UserHasInterest getLastUserHasInterestElement(final Collection<UserHasInterest> c) {
	    final Iterator<UserHasInterest> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (UserHasInterest) lastElement;
	}
	
	public UserHasMusic getLastUserHasMusicElement(final Collection<UserHasMusic> c) {
	    final Iterator<UserHasMusic> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (UserHasMusic) lastElement;
	}

	//--------------------------------------------------------------------------------------
	// Request mapping
	//--------------------------------------------------------------------------------------
	/**
	 * Shows a list with all the occurrences of User found in the database
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping()
	public String list(Model model) {
		log("Action 'list'");
		List<User> list = userService.findAll();
		model.addAttribute(MAIN_LIST_NAME, list);		
		return JSP_LIST;
	}

	public User getLastElement(final Collection<User> c) {
	    final Iterator<User> itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return (User) lastElement;
	}
	
	/**
	 * Shows a form page in order to create a new User
	 * @param model Spring MVC model
	 * @return
	 */
	@RequestMapping("/form")
	public String formForCreate(Model model) {
		log("Action 'formForCreate'");
		//--- Populates the model with a new instance
		User user = new User();	
		
		List<User> allUsers  = userService.findAll();
		User lastuser		 = getLastElement(allUsers);
		
		if(allUsers != null && !allUsers.isEmpty()){
			user.setId(lastuser.getId()+1);
		}
		else{
			user.setId(0);
		}
		
		populateModel( model, user, FormMode.CREATE);
		return JSP_FORM;
	}

	/**
	 * Shows a form page in order to update an existing User
	 * @param model Spring MVC model
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/form/account")
	public String formForUpdate(Model model ) {
		log("Action 'formForUpdate'");

		// On recupere le user connecté
		try{
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user 			= userService.findByUsername(auth.getName());	
			
			if(user != null){
				populateModel( model, user, FormMode.UPDATE);		
				return JSP_FORM;	
			}
			else{
				return "redirect:/";
			}

		}
		catch(Exception e){
			return "redirect:/";
		}
			


	}
	
	// Methode pour connecter le user apres la création de son comte
    private void authenticateUserAndSetSession(User user, HttpServletRequest request, String userPassword) {
        String username = user.getUsername();
        
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, userPassword);

        // generate session if one doesn't exist
        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));	
        Authentication authenticatedUser = authenticationManager.authenticate(token);
  
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }

	/**
	 * 'CREATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param user  entity to be created
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/create" ) // GET or POST
	public String create(@Valid User user, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'create'");
		//On crypt le mot de passe
		String password = bindingResult.getFieldValue("password").toString();
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		user.setPassword(hashedPassword);
		try {
			if (!bindingResult.hasErrors()) {
				User userCreated = userService.create(user); 
				UserRoles userRoles = new UserRoles() ;
				/**
				 * 
				 * A la création d'un user, on lui affecte le role ROLE_PASSENGER
				 *  On créé donc un nouveau UserRoles et on lui set un id, l'username et le role ROLE_PASSENGER
				 *  On attribut un id à la nouvelle entitée UserRoles
				 *  Si il n'existe pas encore d'entité, c'est donc la première et on lui affecte l'id 0
				 */
				List<UserRoles> allUsersRoles  = userRolesService.findAll();
				if(allUsersRoles != null && !allUsersRoles.isEmpty()){
					UserRoles lastUserRoles		 = getUserRolesLastElement(allUsersRoles);
					userRoles.setId(lastUserRoles.getId()+1);	
				}
				else{
					userRoles.setId(0);
				}
				
				userRoles.setRole("ROLE_PASSENGER");
				userRoles.setUsername(userCreated.getUsername());
				userRolesService.create(userRoles); 
				
			   // On connect le nouveau user et on redirige sur la home page
			   authenticateUserAndSetSession(user, httpServletRequest, password);
			   
			   return "redirect:/";
				
			} else {
				populateModel( model, user, FormMode.CREATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			log("Action 'create' : Exception - " + e.getMessage() );
			messageHelper.addException(model, "user.error.create", e);
			populateModel( model, user, FormMode.CREATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'UPDATE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param user  entity to be updated
	 * @param bindingResult Spring MVC binding result
	 * @param model Spring MVC model
	 * @param redirectAttributes Spring MVC redirect attributes
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/update" ) // GET or POST
	public String update(@Valid User user, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
		log("Action 'update'");
		try {
			if (!bindingResult.hasErrors()) {
				//--- Perform database operations
				User userSaved = userService.update(user);
				model.addAttribute(MAIN_ENTITY_NAME, userSaved);
				//--- Set the result message
				messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
				log("Action 'update' : update done - redirect");
				return redirectToForm(httpServletRequest, user.getId());
			} else {
				log("Action 'update' : binding errors");
				populateModel( model, user, FormMode.UPDATE);
				return JSP_FORM;
			}
		} catch(Exception e) {
			messageHelper.addException(model, "user.error.update", e);
			log("Action 'update' : Exception - " + e.getMessage() );
			populateModel( model, user, FormMode.UPDATE);
			return JSP_FORM;
		}
	}

	/**
	 * 'DELETE' action processing. <br>
	 * This action is based on the 'Post/Redirect/Get (PRG)' pattern, so it ends by 'http redirect'<br>
	 * @param redirectAttributes
	 * @param id  primary key element
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}") // GET or POST
	public String delete(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Integer id) {
		log("Action 'delete'" );
		try {
			userService.delete( id );
			//--- Set the result message
			messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));	
		} catch(Exception e) {
			messageHelper.addException(redirectAttributes, "user.error.delete", e);
		}
		
		// On vide la session pour virer le user
		SecurityContextHolder.clearContext();
	    HttpSession session = request.getSession(false);
	    if (session != null) {
	            session.invalidate();
	    }
	    
		return "redirect:/";
	}
	
	/**
	 * Les deux controlleurs suivant permettrons de faire passer un user de Passager à Conducteur
	 * 
	 * Pour qu'un passager devienne conducteur, on va :
	 * - Créer une voiture
	 * - Mettre à jour le user avec sa voiture et son num de permis
	 * - Enfin mettre le role ROLE_DRIVER
	 */
	@RequestMapping(value = "/driver/car")
	public String formForDriverCar(Model model, RedirectAttributes redirectAttrs) {
		log("Action 'form Driver 1'");
		
	    redirectAttrs.addAttribute("fromDriver", true);
		return "redirect:/car/form";
	}
	
	@RequestMapping(value = "/driver/end")
	public String formForDriverEnd(Model model) {
		log("Action 'form Driver 2'");
		
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 	= userService.findByUsername(auth.getName());
		
		populateModel( model, user, FormMode.UPDATE);	
		
		// On recupere la voiture, son modele et sa marque
		Car car 			= carService.findById(user.getCarId());
		Carmodel carmodel 	= carmodelService.findCarmodelByCarId(car.getId());
		Brand brand			= brandService.findBrandByCarmodel(carmodel.getId());

		// Et on les renvoie à la vue
		model.addAttribute("car", car);
		model.addAttribute("carmodel", carmodel);
		model.addAttribute("brand", brand);

		// On donne le role ROLE_DRIVER au user
		UserRoles userRoles = new UserRoles() ;
		List<UserRoles> allUsersRoles  = userRolesService.findAll();
		if(allUsersRoles != null && !allUsersRoles.isEmpty()){
			UserRoles lastUserRoles		 = getUserRolesLastElement(allUsersRoles);
			userRoles.setId(lastUserRoles.getId()+1);	
		}
		else{
			userRoles.setId(0);
		}
		
		// On verifie si le user n'est pas deja conducteur
		List<UserRoles> listUserRoles = userRolesService.findByUsername(user.getUsername());
		Boolean isDriver 			  = false ;
		for ( UserRoles role : listUserRoles ) {
			if(role.getRole().equalsIgnoreCase("ROLE_DRIVER")){
				isDriver = true ;
			}
		}
		
		// Si le user n'est pas encore conducteur on le met conducteur
		if(!isDriver){
			userRoles.setRole("ROLE_DRIVER");
			userRoles.setUsername(user.getUsername());
			userRolesService.create(userRoles); 			
		}

		return JSP_DRIVER_FORM;
	}
	
	@RequestMapping(value = "/preference/form")
	public String formUserPreference(Model model) {
		log("Action 'form User Preference'");
		
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 	= userService.findByUsername(auth.getName());
		
		// On recupere la liste des interets et des musiques existant
		List<Interest> interestList = interestService.findAll();
		List<Music> musicList = musicService.findAll();
		
		// On recupere les UserHasMusic liés au user
		List<UserHasMusic> userHasMusicList = userHasMusicService.findByUserId(user.getId()) ;
		// On recupere les UserHasInterest liés au user
		List<UserHasInterest> userHasInterestList = userHasInterestService.findByUserId(user.getId()) ;
		
		//On construit une liste des Interest du User
		List<Interest> userInterestList = new ArrayList<Interest>();
		for(UserHasInterest userHasInterest : userHasInterestList){
			Interest interest = interestService.findById(userHasInterest.getInterestId());
			userInterestList.add(interest);
			// On supprime de la liste totale les Interest du User
			// interestList.remove(interest); ne fonctionne pas, il faut forcement faire un iterator ...
			Iterator<Interest> it = interestList.iterator();
			while (it.hasNext()) {
				Interest interest2 = it.next();
			    if (interest2.getId() == interest.getId()) {
			        it.remove();
			    }
			}
		}
		//On construit une liste des Music du User
		List<Music> userMusicList = new ArrayList<Music>();
		for(UserHasMusic userHasMusic : userHasMusicList){
			Music music = musicService.findById(userHasMusic.getMusicId());
			userMusicList.add(music); 
			// On supprime de la liste totale les Music du User
			// musicList.remove(music); ne fonctionne pas, il faut forcement faire un iterator ...
			Iterator<Music> it = musicList.iterator();
			while (it.hasNext()) {
				Music music2 = it.next();
			    if (music2.getId() == music.getId()) {
			        it.remove();
			    }
			}
		}
		
		// Et on les renvoie à la vue
		model.addAttribute("user", user);
		model.addAttribute("interestList", interestList);
		model.addAttribute("musicList", musicList);
		model.addAttribute("userMusicList", userMusicList);
		model.addAttribute("userInterestList", userInterestList);


		return JSP_PREFERENCE_FORM;
	}
	
	@RequestMapping(value = "/preference/update", method = RequestMethod.POST)
	public String userPreferenceUpdate(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log("Action 'form User Preference'");
		
		// On recupere le user connecté
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user 	= userService.findByUsername(auth.getName());
		
		// On recupere les listes des musiques et des centres d'interet choisies
		String[] musicsList = request.getParameterValues("user_musics[]");
		String[] interestsList = request.getParameterValues("user_interests[]");
		
		// On recupere les bool Smoker, Talker et Listenmusic
		String smoker = request.getParameter("smoker");
		String talker = request.getParameter("talker");
		String listenmusic = request.getParameter("listenmusic");
		
		// On recupere les UserHasInterest liés au user et on les supprime
		List<UserHasInterest> userHasInterestList = userHasInterestService.findByUserId(user.getId()) ;
		for(UserHasInterest userHasInterest : userHasInterestList){
			userHasInterestService.delete(userHasInterest.getUserId(), userHasInterest.getInterestId());
		}
		
		// Puis on recréé les UserHasInterest avec les Interest chosis dans le form
		if(interestsList != null){
			for(String interestId : interestsList){
				UserHasInterest userHasInterest = new UserHasInterest() ;
				
				userHasInterest.setInterestId(Integer.parseInt(interestId));
				userHasInterest.setUserId(user.getId());
				
				userHasInterestService.create(userHasInterest);
			}
		}
		
		
		// On recupere les UserHasMusic liés au user et on les supprime
		List<UserHasMusic> userHasMusicList = userHasMusicService.findByUserId(user.getId()) ;
		for(UserHasMusic userHasMusic : userHasMusicList){
			userHasMusicService.delete(userHasMusic.getUserId(),userHasMusic.getMusicId());
		}
						
		// Puis on recréé les UserHasMusic avec les Music chosis dans le form
		if(musicsList != null){
			for(String musicId : musicsList){
				UserHasMusic userHasMusic = new UserHasMusic() ;
				
				userHasMusic.setMusicId(Integer.parseInt(musicId));
				userHasMusic.setUserId(user.getId());
				
				userHasMusicService.create(userHasMusic);					
			}	
		}
		
		// On met à jour les bool Smoker, Talker et Listenmusic en fonction des parametre recus du form
		if(smoker != null){
			user.setSmoker(true);
		}
		else{
			user.setSmoker(false);
		}
		if(listenmusic != null){
			user.setListenmusic(true);
		}
		else{
			user.setListenmusic(false);
		}
		if(talker != null){
			user.setTalker(true);
		}
		else{
			user.setTalker(false);
		}
		
		// Et on sauvegarde le User
		userService.update(user);		

		
		// Et on les renvoie la vue du form mis à jour avec un message de confirmation
		messageHelper.addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
		return "redirect:/user/preference/form";
	}

}
