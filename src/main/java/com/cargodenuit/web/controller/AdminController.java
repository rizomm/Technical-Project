package com.cargodenuit.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cargodenuit.business.service.BookingService;
import com.cargodenuit.business.service.TripService;
import com.cargodenuit.business.service.UserRolesService;
import com.cargodenuit.business.service.UserService;
import com.cargodenuit.entity.Booking;
import com.cargodenuit.entity.Trip;
import com.cargodenuit.entity.User;
import com.cargodenuit.entity.UserRoles;
 
@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Resource
    private UserRolesService userRolesService;
	
	@Resource
    private UserService userService;
	
	@Resource
    private TripService tripService;
	
	@Resource
    private BookingService bookingService;
 
	@RequestMapping(value = "")
	public ModelAndView indexAdmin() {
 
		ModelAndView model = new ModelAndView("admin/index");
		
		
		// On recupere le user connecté
		Authentication auth 		= SecurityContextHolder.getContext().getAuthentication();
		User user 					= userService.findByUsername(auth.getName());
	    
		// On recupere tous les users
		List<User> allUsers 		= userService.findAll();

		// On recupere tous les trips
		List<Trip> allTrips 		= tripService.findAll();
		
		// On recupere tous les bookings
		List<Booking> allBookings 	= bookingService.findAll();		
		
		
		// On recupere le nombre de trips
		Integer nbTrips 	= allTrips.size() ;
		
		// On recupere le nombre de bookings
		Integer nbBookings 	= allBookings.size() ;
		
		// On recupere le nombre de trip
		Integer nbUsers		= allUsers.size() ;

		//Calcul du nombre de km parcourus
		Integer kmTraveled = 0;
		for(Trip trip : allTrips){
			kmTraveled += trip.getDistance();
		}
		for(Booking booking : allBookings){
			kmTraveled += booking.getDistance();
		}
		
		// Calcul du nombre de drivers
		// Pour tous les users, on regarde si il est DRIVER et on regarde si il a une note supperieur à 3
		// On verifie si le user n'est pas deja conducteur
		Integer nbDrivers 				= 0 ;
		Integer goodEvaluatedDrivers 	= 0 ;
		for(User user2 : allUsers){
			if(user2.getMark() != null){
				if(user2.getMark().intValue() >= 3){
					goodEvaluatedDrivers += 1 ;
				}
			}
			List<UserRoles> listUserRoles = userRolesService.findByUsername(user2.getUsername());
			for ( UserRoles role : listUserRoles ) {
				if(role.getRole().equalsIgnoreCase("ROLE_DRIVER")){
					nbDrivers += 1 ;
				}
			}
		}
		
		// On recupere les 8 dernier bookings
		List<Booking> lastBookings = new ArrayList<Booking>() ;
		Integer i = 0;
		for(Booking book : allBookings){
			if(i<=8){
				lastBookings.add(book);
			}
		}
		
		
		model.addObject("user", user);
		model.addObject("allTrips", allTrips);
		model.addObject("allUsers", allUsers);
		model.addObject("allBookings", allBookings);
		model.addObject("nbTrips", nbTrips);
		model.addObject("nbBookings", nbBookings);
		model.addObject("nbUsers", nbUsers);
		model.addObject("kmTraveled", kmTraveled);
		model.addObject("nbDrivers", nbDrivers);
		model.addObject("goodEvaluatedDrivers", goodEvaluatedDrivers);
		model.addObject("lastBookings", lastBookings);

 
		return model;
 
	}
 
}