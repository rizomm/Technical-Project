package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.MusicEntity;

/**
 * Repository : Music.
 */
public interface MusicJpaRepository extends PagingAndSortingRepository<MusicEntity, Integer> {

}
