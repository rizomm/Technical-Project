package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.UserHasInterestEntity;
import com.cargodenuit.entity.jpa.UserHasInterestEntityKey;

/**
 * Repository : UserHasInterest.
 */
public interface UserHasInterestJpaRepository extends PagingAndSortingRepository<UserHasInterestEntity, UserHasInterestEntityKey> {

}
