package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.FuelEntity;

/**
 * Repository : Fuel.
 */
public interface FuelJpaRepository extends PagingAndSortingRepository<FuelEntity, Integer> {

}
