package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.entity.jpa.TripEntity;

/**
 * Repository : Trip.
 */
public interface TripJpaRepository extends PagingAndSortingRepository<TripEntity, Integer> {

	Iterable<TripEntity> findByUserId(Integer id);

}
