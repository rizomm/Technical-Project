package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.entity.jpa.KmcostEntity;

/**
 * Repository : Kmcost.
 */
public interface KmcostJpaRepository extends PagingAndSortingRepository<KmcostEntity, Integer> {

	KmcostEntity findByFuelId(Integer fuelid);

}
