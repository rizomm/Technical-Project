package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.entity.jpa.UserEntity;

/**
 * Repository : User.
 */
public interface UserJpaRepository extends PagingAndSortingRepository<UserEntity, Integer> {

	UserEntity findByUsername(String email);
}
