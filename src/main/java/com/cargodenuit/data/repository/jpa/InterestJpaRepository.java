package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.InterestEntity;

/**
 * Repository : Interest.
 */
public interface InterestJpaRepository extends PagingAndSortingRepository<InterestEntity, Integer> {

}
