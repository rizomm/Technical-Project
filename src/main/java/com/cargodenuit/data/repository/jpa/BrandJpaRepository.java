package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.entity.jpa.BrandEntity;

/**
 * Repository : Brand.
 */
public interface BrandJpaRepository extends PagingAndSortingRepository<BrandEntity, Integer> {

}
