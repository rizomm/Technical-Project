package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.entity.jpa.BookingEntity;

/**
 * Repository : Booking.
 */
public interface BookingJpaRepository extends PagingAndSortingRepository<BookingEntity, Integer> {

	Iterable<BookingEntity> findByUserId(Integer id);

}
