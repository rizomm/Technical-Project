package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.UserHasMusicEntity;
import com.cargodenuit.entity.jpa.UserHasMusicEntityKey;

/**
 * Repository : UserHasMusic.
 */
public interface UserHasMusicJpaRepository extends PagingAndSortingRepository<UserHasMusicEntity, UserHasMusicEntityKey> {

}
