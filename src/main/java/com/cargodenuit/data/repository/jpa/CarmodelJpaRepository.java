package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cargodenuit.business.service.BrandService;
import com.cargodenuit.entity.Brand;
import com.cargodenuit.entity.jpa.BrandEntity;
import com.cargodenuit.entity.jpa.CarmodelEntity;

/**
 * Repository : Carmodel.
 */
public interface CarmodelJpaRepository extends PagingAndSortingRepository<CarmodelEntity, Integer> {

   
	Iterable<CarmodelEntity> findByBrand(BrandEntity brand);
	

}
