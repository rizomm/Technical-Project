package com.cargodenuit.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.cargodenuit.entity.jpa.CarEntity;

/**
 * Repository : Car.
 */
public interface CarJpaRepository extends PagingAndSortingRepository<CarEntity, Integer> {

}
